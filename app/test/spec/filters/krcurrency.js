'use strict';

describe('Filter: krCurrency', function () {

  // load the filter's module
  beforeEach(module('4workApp'));

  // initialize a new instance of the filter before each test
  var krCurrency;
  beforeEach(inject(function ($filter) {
    krCurrency = $filter('krCurrency');
  }));

  it('should return the input prefixed with "krCurrency filter:"', function () {
    var text = 'angularjs';
    expect(krCurrency(text)).toBe('krCurrency filter: ' + text);
  });

});
