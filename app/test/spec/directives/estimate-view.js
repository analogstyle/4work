'use strict';

describe('Directive: estimateView', function () {

  // load the directive's module
  beforeEach(module('4workApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<estimate-view></estimate-view>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the estimateView directive');
  }));
});
