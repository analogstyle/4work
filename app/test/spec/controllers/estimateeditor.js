'use strict';

describe('Controller: EstimateEditorCtrl', function () {

  // load the controller's module
  beforeEach(module('4workApp'));

  var EstimateEditorCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EstimateEditorCtrl = $controller('EstimateEditorCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
