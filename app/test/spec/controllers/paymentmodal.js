'use strict';

describe('Controller: PaymentModalCtrl', function () {

  // load the controller's module
  beforeEach(module('4workApp'));

  var PaymentModalCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PaymentModalCtrl = $controller('PaymentModalCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
