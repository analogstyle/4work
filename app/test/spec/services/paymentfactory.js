'use strict';

describe('Service: PaymentFactory', function () {

  // load the service's module
  beforeEach(module('4workApp'));

  // instantiate service
  var PaymentFactory;
  beforeEach(inject(function (_PaymentFactory_) {
    PaymentFactory = _PaymentFactory_;
  }));

  it('should do something', function () {
    expect(!!PaymentFactory).toBe(true);
  });

});
