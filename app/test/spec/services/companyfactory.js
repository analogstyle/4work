'use strict';

describe('Service: CompanyFactory', function () {

  // load the service's module
  beforeEach(module('4workApp'));

  // instantiate service
  var CompanyFactory;
  beforeEach(inject(function (_CompanyFactory_) {
    CompanyFactory = _CompanyFactory_;
  }));

  it('should do something', function () {
    expect(!!CompanyFactory).toBe(true);
  });

});
