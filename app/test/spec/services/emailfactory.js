'use strict';

describe('Service: EmailFactory', function () {

  // load the service's module
  beforeEach(module('4workApp'));

  // instantiate service
  var EmailFactory;
  beforeEach(inject(function (_EmailFactory_) {
    EmailFactory = _EmailFactory_;
  }));

  it('should do something', function () {
    expect(!!EmailFactory).toBe(true);
  });

});
