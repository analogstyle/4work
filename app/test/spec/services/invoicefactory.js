'use strict';

describe('Service: InvoiceFactory', function () {

  // load the service's module
  beforeEach(module('4workApp'));

  // instantiate service
  var InvoiceFactory;
  beforeEach(inject(function (_InvoiceFactory_) {
    InvoiceFactory = _InvoiceFactory_;
  }));

  it('should do something', function () {
    expect(!!InvoiceFactory).toBe(true);
  });

});
