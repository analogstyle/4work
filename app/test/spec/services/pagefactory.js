'use strict';

describe('Service: PageFactory', function () {

  // load the service's module
  beforeEach(module('4workApp'));

  // instantiate service
  var PageFactory;
  beforeEach(inject(function (_PageFactory_) {
    PageFactory = _PageFactory_;
  }));

  it('should do something', function () {
    expect(!!PageFactory).toBe(true);
  });

});
