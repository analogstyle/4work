'use strict';

describe('Service: EstimateFactory', function () {

  // load the service's module
  beforeEach(module('4workApp'));

  // instantiate service
  var EstimateFactory;
  beforeEach(inject(function (_EstimateFactory_) {
    EstimateFactory = _EstimateFactory_;
  }));

  it('should do something', function () {
    expect(!!EstimateFactory).toBe(true);
  });

});
