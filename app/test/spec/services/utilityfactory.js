'use strict';

describe('Service: UtilityFactory', function () {

  // load the service's module
  beforeEach(module('4workApp'));

  // instantiate service
  var UtilityFactory;
  beforeEach(inject(function (_UtilityFactory_) {
    UtilityFactory = _UtilityFactory_;
  }));

  it('should do something', function () {
    expect(!!UtilityFactory).toBe(true);
  });

});
