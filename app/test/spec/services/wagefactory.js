'use strict';

describe('Service: WageFactory', function () {

  // load the service's module
  beforeEach(module('4workApp'));

  // instantiate service
  var WageFactory;
  beforeEach(inject(function (_WageFactory_) {
    WageFactory = _WageFactory_;
  }));

  it('should do something', function () {
    expect(!!WageFactory).toBe(true);
  });

});
