'use strict';

describe('Service: CalculatorFactory', function () {

  // load the service's module
  beforeEach(module('4workApp'));

  // instantiate service
  var CalculatorFactory;
  beforeEach(inject(function (_CalculatorFactory_) {
    CalculatorFactory = _CalculatorFactory_;
  }));

  it('should do something', function () {
    expect(!!CalculatorFactory).toBe(true);
  });

});
