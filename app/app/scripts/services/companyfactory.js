'use strict';

/**
 * @ngdoc service
 * @name 4workApp.CompanyFactory
 * @description
 * # CompanyFactory
 * Factory in the 4workApp.
 */
angular.module('4workApp')
  .factory('CompanyFactory', function ($http, $resource, API_URL, AuthTokenFactory) {
    var Company = $resource(API_URL + '/company/:id', { id: '@id' }, {
      update: { method: 'PUT' },
      count : { method: 'GET', url: API_URL + '/company/count' }
    });

    // Public API here
    return {
      getMyCompany: function () {
        return $http
          .post(API_URL + '/myCompany')
          .then(function (res) {
            return res.data;
          });
      },

      create: function (company) {
        var accessToken = AuthTokenFactory.getToken();
        if ( !accessToken ) {
          return alert('로그인이 필요합니다.');
        }

        company.members = [ accessToken ];

        return new Company(company).$save();
      },

      update: function (company) {
        return Company.update({ id: company.id }, company).$promise;
      },

      count: function (option) {
        return Company.count(option);
      }
    };
  });
