'use strict';

/**
 * @ngdoc service
 * @name 4workApp.PageFactory
 * @description
 * # PageFactory
 * Factory in the 4workApp.
 */
angular.module('4workApp')
  .factory('PageFactory', function () {
    var isNavShown = true;

    return {
      navStatus: function () {
        return isNavShown;
      },

      toggleNavStatus: function (status) {
        if ( status !== undefined )
          isNavShown = status;
        else
          isNavShown = !isNavShown;
      }
    };
  });
