'use strict';

/**
 * @ngdoc service
 * @name 4workApp.CalculatorFactory
 * @description
 * # CalculatorFactory
 * Factory in the 4workApp.
 */
angular.module('4workApp')
  .factory('CalculatorFactory', function (UtilityFactory) {
    return {
      calculateTarget: function (target, estimate) {
        if ( !target || !estimate ) return;

        return parseInt(target) - this.total(estimate);
      },

      totalDirectExpense: function (direct) {
        var sum = 0;

        for ( var i in direct ) {
          var currentDirect = direct[i];

          for ( var j in currentDirect.data ) {
            var currentData = currentDirect.data[j];
            sum += ( currentData.daily * currentData.number * currentData.support * currentData.participation/100 ) || 0;
          }
        }

        return sum;
      },

      totalIndirectExpense: function (estimate) {
        var sum = 0;
        var directExpense = this.totalDirectExpense(estimate.expenses.direct);

        for ( var i in estimate.expenses.indirect ) {
          var currentIndirect = estimate.expenses.indirect[i];
          if ( currentIndirect.mode === 'RATIO' )
            sum += directExpense * currentIndirect.ratio/100;
          else
            sum += parseInt(currentIndirect.amount);
        }

        return sum;
      },

      subTotal: function (estimate) {
        if ( estimate.type === 'DEVELOPMENT' ) {
          return parseInt(this.totalExpenses(estimate)) || 0;
        } else if ( estimate.type === 'PRODUCT' ) {
          var subTotal = 0;

          for ( var i in estimate.products ) {
            var currentProduct = estimate.products[i];

            var value = ( currentProduct.tax ) ? currentProduct.price * (1+currentProduct.tax/100) * currentProduct.quantity : currentProduct.price * currentProduct.quantity;

            if ( isNaN(value) ) value = 0;

            subTotal += value;
          }

          return subTotal;
        }
      },

      total: function (estimate) {
        var total = 0;
        var subTotal = 0;
        var discount = 0;
        var shipping = 0;
        var adjustment = 0;

        if ( estimate.type === 'DEVELOPMENT' ) {
          subTotal = parseInt(this.totalExpenses(estimate)) || 0;
          discount = parseInt(estimate.discount/100*subTotal) || 0;
          shipping = parseInt(estimate.shipping) || 0;
          adjustment = parseInt(estimate.adjustment) || 0;
        } else if ( estimate.type === 'PRODUCT' ) {
          subTotal = parseInt(this.subTotal(estimate)) || 0;
          discount = parseInt(estimate.discount/100*subTotal) || 0;
          shipping = parseInt(estimate.shipping) || 0;
          adjustment = parseInt(estimate.adjustment) || 0;
        }

        return subTotal + shipping - discount - adjustment;
      },

      indirectSubtotal: function (estimate, expense) {
        var directExpense = this.totalDirectExpense(estimate.expenses.direct);

        return ( expense.ratio ) ? directExpense * expense.ratio/100 : expense.amount ;
      },

      totalExpenses: function (estimate) {
        var directExpense = this.totalDirectExpense(estimate.expenses.direct);
        var indirectExpense = this.totalIndirectExpense(estimate);

        return directExpense + indirectExpense;
      },

      totalDirectNumber: function (direct) {
        var sum = 0;

        for ( var i in direct ) {
          var currentDirect = direct[i];
          for ( var j in currentDirect.data ) sum += currentDirect.data[j].number;
        }

        return sum;
      },

      totalDirectSupport: function (direct) {
        var sum = 0;

        for ( var i in direct ) {
          var currentDirect = direct[i];
          for ( var j in currentDirect.data ) sum += currentDirect.data[j].support;
        }

        return sum;
      },
    }
  });
