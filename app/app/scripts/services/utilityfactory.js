'use strict';

/**
 * @ngdoc service
 * @name 4workApp.UtilityFactory
 * @description
 * # UtilityFactory
 * Factory in the 4workApp.
 */
angular.module('4workApp')
  .factory('UtilityFactory', function ($filter, $http, API_URL, CLIENT_URL) {
    var prefixs = ['http://', 'https://'];

    return {
      clone: function (object) {
        if (null == object || "object" != typeof object) return object;
        var copy = object.constructor();
        for (var attr in object) {
            if (object.hasOwnProperty(attr)) copy[attr] = object[attr];
        }
        return copy;
      },

      generateMailContent: function (user, company, attachment, type) {
        var content = '';

        content += '<p>안녕하세요. ' + company.name;
        if ( user.name ) content += ' ' + user.name;
        content += ' 입니다.</p>';

        if ( type === 'payment' )
          content += '<p>함께 일하게 해주신 것에 진심으로 감사드립니다.</p>';
        else
          content += '<p>저희 ' + company.name + '으로 문의주신 것에 감사 인사드립니다.</p>';

        if ( type === 'payment' )
          content += '<p>요청하신 ' + $filter('translate')('RECEIPT') + '은 아래 링크를 클릭하여 ';
        else
          content += '<p>요청하신 ' + $filter('translate')(type.toUpperCase()) + ' ' + attachment.no + ' 는 아래 링크를 클릭하여 ';

        if ( type === 'estimate' )
          content += ' 직접 열람 및 승인하실 수 있습니다.</p>';
        else if ( type === 'invoice' )
          content += ' 직접 열람 및 지불하실 수 있습니다.</p>';
        else if ( type === 'payment' )
          content += ' 확인하실 수 있습니다</p>';

        content += '<p><strong><a href="' + CLIENT_URL + '/' + type + '/' + attachment.id + '" target="_blank">클릭하여 ';

        if ( type === 'payment' )
           content += $filter('translate')('RECEIPT') + ' 열람하기</a></strong></p>';
        else
          content += $filter('translate')(type.toUpperCase()) + ' 열람하기</a></strong></p>';

        content += '<p>기타 문의사항 및 궁금하신 점은 아래 연락처로 문의주시기 바랍니다.</p>';
        content += '<p>감사합니다.</p>';
        content += '<p>&nbsp;</p>';

        if ( user.name  ) content += '<p>' + user.name + '<br />';
        if ( user.phone ) content += user.phone + '<br />'
        content += user.email + '</p>'

        return content;
      },

      generateMailTitle: function (user, company, type) {
        var subject = company.name;

        if ( user.name ) subject += ' ' + user.name + ' 님께서 ';
        else             subject += ' 에서 ';

        if ( type === 'payment') {
          subject += $filter('translate')('RECEIPT');
        } else {
          subject += $filter('translate')(type.toUpperCase());
        }
        subject += ( type === 'payment' ) ? '을 보냈습니다.' : '를 보냈습니다.';

        return subject;
      },

      getUrl: function (url) {
        if ( !url || url.length === 0 ) return;

        url.trim();

        var needPrefix = false;

        for ( var i in prefixs ) {
          if ( url.substr(0, prefixs[i].length) === prefixs[i] ) {
            needPrefix = false;
            break;
          }

          needPrefix = true
        }

        if ( needPrefix ) url = 'http://' + url;

        return url;
      },

      validateEmail: function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return re.test(email);
      },

      isEmailDuplicated: function (emails, email) {
        for ( var i in emails ) {
          if ( emails[i] === email ) {
            return true;
          }
        }

        return false;
      },

      isProductDuplicated: function (products, selected) {
        for ( var i in products ) {
          if ( products[i].id === selected.id ) {
            return true;
          }
        }

        return false;
      },

      showInvalidAttributes: function (err) {
        var alertMessage = '';

        if ( err.data.hasOwnProperty('invalidAttributes') ) {
          for ( var i in err.data.invalidAttributes )
            alertMessage += i + ', ';

          alert(alertMessage);
        } else {
          return alert('견적서 생성 중 오류가 발생하였습니다.');
        }
      },

      getTotal: function (estimate) {
        var subTotal = parseInt(this.getSubTotal(estimate)) || 0;
        var discount = parseInt(estimate.discount/100*subTotal) || 0;
        var shipping = parseInt(estimate.shipping) || 0;
        var adjustment = -parseInt(estimate.adjustment) || 0;

        return subTotal - discount + shipping + adjustment;
      },

      getSubTotal: function (estimate) {
        var subTotal = 0;

        for ( var i in estimate.products ) {
          var currentProduct = estimate.products[i];

          var value = ( currentProduct.tax ) ? currentProduct.price * (1+currentProduct.tax/100) * currentProduct.quantity : currentProduct.price * currentProduct.quantity;

          if ( isNaN(value) ) value = 0;

          subTotal += value;
        }

        return subTotal;
      },

      errorHandler: function (err) {
        console.log('ERROR:', err);

        alert(err.data + ' 오류를 발견하였습니다.');
      },

      checkPortalDuplicate: function (url) {
        return $http
          .get(API_URL + '/check/url/' + url)
          .then(function (res) {
            return res.data;
          });
      }
    };

  });
