'use strict';

/**
 * @ngdoc service
 * @name 4workApp.AuthTokenFactory
 * @description
 * # AuthTokenFactory
 * Factory in the 4workApp.
 */
angular.module('4workApp')
  .factory('AuthTokenFactory', function ($window) {
    var local = $window.localStorage;
    var session = $window.sessionStorage;

    var key = 'auth-token';

    return {
      getToken: function () {
        return local.getItem(key);
      },

      setToken: function (token) {
        if (token)
          local.setItem(key, token);
        else
          local.removeItem(key);
      }
    };
  });
