'use strict';

/**
 * @ngdoc service
 * @name 4workApp.EstimateFactory
 * @description
 * # EstimateFactory
 * Factory in the 4workApp.
 */
angular.module('4workApp')
  .factory('EstimateFactory', function ($resource, $http, API_URL) {
    var Estimate = $resource(API_URL + '/estimate/:id', { id: '@id' }, {
      update: { method: 'PUT' },
      count : { method: 'GET', url: API_URL + '/estimate/count' }
    });

    // Public API here
    return {
      getList: function (option) {
        return Estimate.query(option);
      },

      getOne: function (id)  {
        return Estimate.get({ id: id });
      },

      approve: function (id, status) {
        return $http
          .get(API_URL + '/estimate/approve/' + status)
          .then(function (res) {
            return res.data;
          });
        // return Estimate.update({ id: id }, { status: status });
      },

      create: function (data) {
        return new Estimate(data).$save();
      },

      edit: function (data) {
        return Estimate.update({ id: data.id }, data);
      },

      delete: function (id) {
        return Estimate.remove(id);
      },

      getEstimateNo: function () {
        return $http
          .get(API_URL + '/estimate/index')
          .then(function (res) {
            return res.data;
          });
      },

      count: function (option) {
        return Estimate.count(option);
      }
    };
  });
