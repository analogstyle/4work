'use strict';

/**
 * @ngdoc service
 * @name 4workApp.ProductFactory
 * @description
 * # ProductFactory
 * Factory in the 4workApp.
 */
angular.module('4workApp')
  .factory('ProductFactory', function ($resource, API_URL) {
    var Product = $resource(API_URL + '/product/:id', { id: '@id' }, {
      update: { method: 'PUT' }
    });

    return {
      getList: function (option) {
        return Product.query(option);
      },

      getOne: function (id) {
        return Product.get({ id: id });
      },

      delete: function (id) {
        return Product.remove(id);
      },

      add: function (productData) {
        return new Product(productData).$save();
      },

      edit: function (productData) {
        console.log(productData);

        return Product.update({ id: productData.id }, productData);
      },

      count: function (option) {
        return Product.count(option);
      }
    };
  });
