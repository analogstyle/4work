'use strict';

/**
 * @ngdoc service
 * @name 4workApp.EmailFactory
 * @description
 * # EmailFactory
 * Factory in the 4workApp.
 */
angular.module('4workApp')
  .factory('EmailFactory', function ($resource, API_URL) {
    var Email = $resource(API_URL + '/email/:id', { id: '@id' }, {
      update: { method: 'PUT' },
      count : { method: 'GET', url: API_URL + '/email/count' }
    });

    return {
      getList: function (option) {
        return Email.query(option);
      },

      getOne: function (id) {
        return Email.get({ id: id });
      },

      delete: function (id) {
        return Email.remove(id);
      },

      send: function (data) {
        return new Email(data).$save();
      },

      edit: function (data) {
        return Email.update({ id: data.id }, data);
      },

      count: function (option) {
        return Email.count(option);
      }
    };
  });
