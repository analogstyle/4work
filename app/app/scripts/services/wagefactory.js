'use strict';

/**
 * @ngdoc service
 * @name 4workApp.WageFactory
 * @description
 * # WageFactory
 * Factory in the 4workApp.
 */
angular.module('4workApp')
  .factory('WageFactory', function () {
    var thisYear = new Date().getFullYear() - 1;

    var wageList = {
      2014: [{
        rate: '기술사',
        daily: 408995,
        monthly: 408995*21
      }, {
        rate: '특급기술자',
        daily: 376262,
        monthly: 376262*21
      }, {
        rate: '고급기술자',
        daily: 272075,
        monthly: 272075*21
      }, {
        rate: '중급기술자',
        daily: 221371,
        monthly: 221371*21
      }, {
        rate: '초급기술자',
        daily: 189174,
        monthly: 189174*21
      }, {
        rate: '고급기능사',
        daily: 172384,
        monthly: 172384*21
      }, {
        rate: '중급기능사',
        daily: 140531,
        monthly: 140531*21
      }, {
        rate: '초급기능사',
        daily: 116756,
        monthly: 116756*21
      }, {
        rate: '자료입력원',
        daily: 111487,
        monthly: 111487*21
      }]
    };

    return {
      getWages: function (wages) {
        if ( !wages ) return wageList[thisYear];

        var wageArray = [];

        for ( var i in wages ) {
          for ( var j in wageList[thisYear] ) {
            var currentWagesList = wageList[thisYear];[i];
            if ( currentWagesList.rate === wages[i] ) wageArray.push(currentWagesList);
          }
        }

        return wageArray;
      },

      getWage: function (rate) {
        for ( var i in wageList[thisYear] ) {
          if ( wageList[thisYear][i].rate === rate ) return wageList[thisYear][i].daily;
        }

        return undefined;
      }
    };
  });
