'use strict';

/**
 * @ngdoc service
 * @name 4workApp.CustomerFactory
 * @description
 * # CustomerFactory
 * Factory in the 4workApp.
 */
angular.module('4workApp')
  .factory('CustomerFactory', function ($resource, API_URL) {
    var Customer = $resource(API_URL + '/customer/:id', { id: '@id' }, {
      update: { method: 'PUT' },
      count : { method: 'GET', url: API_URL + '/customer/count' }
    });

    return {
      getList: function (option) {
        return Customer.query(option);
      },

      getOne: function (id) {
        return Customer.get({ id: id });
      },

      delete: function (id) {
        return Customer.remove(id);
      },

      add: function (customerData) {
        return new Customer(customerData).$save();
      },

      edit: function (customerData) {
        return Customer.update({ id: customerData.id }, customerData);
      },

      count: function (option) {
        return Customer.count(option);
      }
    };
  });
