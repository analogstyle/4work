'use strict';

/**
 * @ngdoc service
 * @name 4workApp.InvoiceFactory
 * @description
 * # InvoiceFactory
 * Factory in the 4workApp.
 */
angular.module('4workApp')
  .factory('InvoiceFactory', function ($resource, $http, API_URL) {
    var Invoice = $resource(API_URL + '/invoice/:id', { id: '@id' }, {
      update: { method: 'PUT' },
      count : { method: 'GET', url: API_URL + '/invoice/count' }
    });

    // Public API here
    return {
      getList: function (option) {
        return Invoice.query(option);
      },

      getOne: function (id)  {
        return Invoice.get({ id: id });
      },

      approve: function (id) {
        return Invoice.update({ id: id }, { isApproved: true });
      },

      create: function (data) {
        return new Invoice(data).$save();
      },

      edit: function (data) {
        return Invoice.update({ id: data.id }, data);
      },

      delete: function (id) {
        return Invoice.remove(id);
      },

      getInvoiceNo: function () {
        return $http
          .get(API_URL + '/invoice/index')
          .then(function (res) {
            return res.data;
          });
      },

      count: function (option) {
        return Invoice.count(option);
      }
    };
  });
