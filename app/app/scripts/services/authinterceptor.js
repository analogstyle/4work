'use strict';

/**
 * @ngdoc service
 * @name 4workApp.AuthInterceptor
 * @description
 * # AuthInterceptor
 * Factory in the 4workApp.
 */
angular.module('4workApp')
  .factory('AuthInterceptor', function (AuthTokenFactory) {
    return {
      request: function AddToken (config) {
        var token = AuthTokenFactory.getToken();

        if (token) {
          // config.body.token = token;
          config.headers = config.headers || {};
          config.headers.Authorization = 'AccessToken ' + token;
        }

        return config;
      }
    };
  });
