'use strict';

/**
 * @ngdoc service
 * @name 4workApp.UserFactory
 * @description
 * # UserFactory
 * Factory in the 4workApp.
 */
angular.module('4workApp')
  .factory('UserFactory', function ($http, $resource, API_URL, AuthTokenFactory) {
    var User = $resource(API_URL + '/user/', { id: '@id' });

    return {
      getStats: function () {
        return $http
          .get(API_URL+'/stats')
          .then(function success (res) {
            return res.data;
          });
      },

      requestToken: function (email) {
        return $http
          .get(API_URL+'/token/request/'+email);
      },

      loginWithToken: function (data) {
        return $http
          .get(API_URL+'/token/login?email='+data.email+'&token='+data.token)
          .then(function success (res) {
            var token = res.data[0];

            AuthTokenFactory.setToken(token.id);
            return token;
          });
      },

      signup: function (data) {
        return $http
          .post(API_URL+'/user', data);
      },

      login: function (data) {
        return $http
          .post(API_URL+'/login', data)
          .then(function success (res) {
            var token = res.data;

            AuthTokenFactory.setToken(token.id);
            return token;
          });
      },

      getUser: function () {
        var token = AuthTokenFactory.getToken();
        if ( !token ) throw new Error('TOKEN_NOT_FOUND');

        return $http
          .post(API_URL+'/token/retrieve', { token: token })
          .then(function (res) {
            var user = res.data;

            return user;
          })
          .catch(function (err) {
            console.log('ERROR:', err);

            // AuthTokenFactory.setToken();
            return err;
          });
      },

      logout: function () {
        AuthTokenFactory.setToken();
      },

      update: function (user) {
        return $http
          .post(API_URL+'/profile', user)
          .then(function (res) {
            return res.data;
          });
      }
    };
  });
