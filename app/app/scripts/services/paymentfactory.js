'use strict';

/**
 * @ngdoc service
 * @name 4workApp.PaymentFactory
 * @description
 * # PaymentFactory
 * Factory in the 4workApp.
 */
angular.module('4workApp')
  .factory('PaymentFactory', function ($resource, API_URL) {
    var Payment = $resource(API_URL + '/payment/:id', { id: '@id' }, {
      update: { method: 'PUT' },
      count : { method: 'GET', url: API_URL + '/payment/count' }
    });

    return {
      getList: function (option) {
        return Payment.query(option);
      },

      getOne: function (id) {
        return Payment.get({ id: id });
      },

      create: function (data) {
        return new Payment(data).$save();
      },

      update: function (data) {
        data = {
          id: data.id,
          createdAt: data.createdAt,
          amount: data.amount,
          bankCharges: data.bankCharges,
          type: data.type,
          description: data.description,
          sendThank: data.sendThank
        };

        return Payment.update({ id: data.id }, data);
      },

      delete: function (id) {
        return Payment.remove(id);
      },

      count: function (option) {
        return Payment.count(option);
      }
    };
  });
