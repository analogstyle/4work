'use strict';

/**
 * @ngdoc filter
 * @name 4workApp.filter:krCurrency
 * @function
 * @description
 * # krCurrency
 * Filter in the 4workApp.
 */
angular.module('4workApp')
  .filter('krCurrency', function ($filter, $locale) {
    var filter = $filter('currency');
    var format = $locale.NUMBER_FORMATS;

    return function (amount) {
      if ( !amount ) amount = 0;

      var value = filter(amount, '');
      var sep = value.indexOf(format.DECIMAL_SEP);

      if ( amount >= 0 ) {
        return '₩' + value.substring(0, sep);
      }

      return '-₩' + value.substring(1, sep);
    };
  });
