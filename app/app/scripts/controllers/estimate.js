'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:EstimateCtrl
 * @description
 * # EstimateCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('EstimateCtrl', function ($scope, $routeParams, $q, $location, UtilityFactory, EstimateFactory, ProductFactory, CustomerFactory, InvoiceFactory, PageFactory, CalculatorFactory) {

    Initiate();

    $scope.Calculator = CalculatorFactory;

    $scope.isExpired = function () {
      // return true;

      return ( $scope.navless === true && $scope.estimate.expiredAt && new Date().getTime() > new Date($scope.estimate.expiredAt).getTime() );
    };

    $scope.pageChanged = function () {
      $scope.estimates = EstimateFactory.getList({ page: $scope.currentPage });
    };

    $scope.deleteEstimate = function (id) {
      if ( !confirm('정말로 삭제하시겠습니까?') ) return;

      EstimateFactory
        .delete({ id: id }).$promise
        .then(function (deleted) {
          alert('삭제되었습니다.');
          $location.path('/estimate');
        });
    };

    $scope.changeStatus = function (status) {
      var confirmText = '';

      switch ( status ) {
        case 'APPROVED':
          confirmText = '견적서를 승인하시겠습니까?';
          break;
        case 'CANCELLED':
          confirmText = '견적서를 반려하시겠습니까?';
          break;
        default:
          return;
      }

      if ( !confirm(confirmText) ) return;

      EstimateFactory
        .approve($scope.estimate.id, status).$promise
        .then(function (estimate) {
          $scope.estimate = estimate;

          alert('처리되었습니다.');
        })
        .catch(function (err) {
          console.log('ERROR:', err);
        });
    };

    $scope.goTo = function (id) {
      if ($scope.currentPage)
        $location.search('page', $scope.currentPage);

      $location.path('/estimate/view/' + id);
    };

    $scope.goBack = function () {
      $location.path('/invoice');
    };

    $scope.convertToInvoice = function (data) {
      if ( data.type !== 'PRODUCT' )
        return alert('현재는 제품 판매용 견적서만 변환이 가능합니다.');

      if ( !confirm('변환하시겠습니까?') ) return;

      data.estimate = data.id;
      // data.no = data.no.replace('EST', 'INV');

      delete data.no;
      delete data.id;
      delete data.status;
      delete data.type;
      delete data.createdAt;

      return InvoiceFactory
        .create(data)
        .then(function (invoice) {
          alert('변환되었습니다.');
          $location.path('/invoice/editor/' + invoice.id);
        })
        .catch(UtilityFactory.errorHandler);
    };

    // CHECKBOX
    var updateSelected = function(action, id) {
      if (action === 'add' && $scope.selected.indexOf(id) === -1) {
        $scope.selected.push(id);
      }
      if (action === 'remove' && $scope.selected.indexOf(id) !== -1) {
        $scope.selected.splice($scope.selected.indexOf(id), 1);
      }
    };

    $scope.updateSelection = function($event, id) {
      var checkbox = $event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      updateSelected(action, id);
    };

    $scope.selectAll = function($event) {
      var checkbox = $event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      for ( var i = 0; i < $scope.estimates.length; i++) {
        var entity = $scope.estimates[i];
        updateSelected(action, entity.id);
      }
    };

    $scope.getSelectedClass = function(entity) {
      return $scope.isSelected(entity.id) ? 'selected' : '';
    };

    $scope.isSelected = function(id) {
      return $scope.selected.indexOf(id) >= 0;
    };

    $scope.isSelectedAll = function() {
      if ( !$scope.estimates ) return false;
      if ( !$scope.selected ) $scope.selected = [];
      return $scope.selected.length === $scope.estimates.length;
    };
    // CHECKBOX

    function Initiate () {
      $scope.isLoading = true;

      $scope.currentPage = $routeParams.page;
      $scope.alerts = [];
      $scope.opened = [];

      if ( $routeParams.id ) {
        $q
          .all([
            EstimateFactory.getOne($routeParams.id.split('?')[0]),
            EstimateFactory.getList({ page: $scope.currentPage }),
            EstimateFactory.count()
          ])
          .then(function (objects) {
            $scope.estimate = objects[0];
            $scope.estimates = objects[1];
            $scope.page = objects[2];

            loadingDone();
          })
          .catch(loadingDone);
      } else {
        $q
          .all([
            EstimateFactory.getList({ page: $scope.currentPage }),
            EstimateFactory.count()
          ])
          .then(function (objects) {
            $scope.estimates = objects[0];
            $scope.page = objects[1];

            loadingDone();
          })
          .catch(loadingDone);
      }
    };

    function loadingDone (err) {
      if (err) console.log('ERROR:', err);

      if ($scope.navless) PageFactory.toggleNavStatus(false);
      else PageFactory.toggleNavStatus(true);

      $scope.isLoading = false;
    };
  });
