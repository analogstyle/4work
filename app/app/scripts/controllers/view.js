'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:ViewCtrl
 * @description
 * # ViewCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('ViewCtrl', function ($scope, $routeParams, PageFactory, EstimateFactory) {
    $scope.PageFactory = PageFactory;
    EstimateFactory.getOne($routeParams.id).$promise.then(function (estimate) {
      $scope.estimate = estimate;
      $scope.company = estimate.issuedBy;
      $scope.user = estimate.writtenBy;
    });

    $scope.PageFactory.toggleNavStatus(false);

    $scope.print = function() {
      $('#buttons').fadeOut(500, function () {
        window.print();
        $('#buttons').show();
      });
    };

    $scope.approve = function () {
      EstimateFactory
        .approve($routeParams.id)
        .then(function (updated) {
          console.log('UPDATED:', updated);
          alert('승인되었습니다.');
        })
        .catch(function (err) {
          console.log('ERROR:', err);
          alert('오류가 발생하였습니다.');
        });
    };
  });
