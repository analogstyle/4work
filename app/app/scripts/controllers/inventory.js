'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:InventoryCtrl
 * @description
 * # InventoryCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('InventoryCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
