'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:InvoiceCtrl
 * @description
 * # InvoiceCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('InvoiceCtrl', function ($scope, $routeParams, $q, $location, $modal, InvoiceFactory, ProductFactory, CustomerFactory, PageFactory) {

    initiate();

    function initiate () {
      // EDITOR
      $scope.isLoading = true;

      $scope.currentPage = $routeParams.page;
      $scope.page = InvoiceFactory.count();

      $scope.invoice = { createdAt: new Date(), products: [{}], emails: [] };
      $scope.alerts = [];
      $scope.opened = [];

      $q
        .all([
          InvoiceFactory.getList({ page: $scope.currentPage }),
          ProductFactory.getList(),
          CustomerFactory.getList(),
          InvoiceFactory.getInvoiceNo(),
        ])
        .then(function (objects) {
          $scope.invoices = objects[0];
          $scope.products = objects[1];
          $scope.customers = objects[2];
          $scope.invoice.no = objects[3].no;

          if ( $routeParams.id ) {
            InvoiceFactory
              .getOne($routeParams.id).$promise
              .then(function (invoice) {
                $scope.invoice = invoice;
                $scope.company = invoice.company;

                loadingDone();
              });
          } else {
            loadingDone();
          }
        });
    };

    $scope.pageChanged = function () {
      $scope.invoices = InvoiceFactory.getList({ page: $scope.currentPage });
    };

    $scope.deleteInvoice = function (id) {
      if ( !confirm('정말로 삭제하시겠습니까?') ) return;

      InvoiceFactory
        .delete({ id: id }).$promise
        .then(function (deleted) {
          alert('삭제되었습니다.');
          $location.path('/invoice');
        });
    };

    $scope.goTo = function (id) {
      if ($scope.currentPage)
        $location.search('page', $scope.currentPage);

      $location.path('/invoice/view/' + id);
    };

    $scope.goBack = function () {
      $location.path('/invoice');
    };

    $scope.openPaymentForm = function (size) {
      console.log('opening');

      var modalInstance = $modal.open({
        templateUrl: '/views/payment-modal.html',
        controller: 'PaymentModalCtrl',
        size: size,
        resolve: {
          object: function () {
            return $scope.invoice;
          }
        }
      });

      modalInstance.result.then(function (payment) {
        $scope.invoice = payment.invoice;
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    };

    $scope.saveInvoice = function (invoice, send) {
      if ( send ) invoice.status = 'SENT';

      if ( invoice.id ) {
        console.log('edit');

        return InvoiceFactory
          .edit(invoice).$promise
          .then(function (invoice) {
            $scope.invoice = invoice;
            alert('저장되었습니다.');
          })
          .catch(function (err) {
            console.log('ERROR:', err);
          });
      } else {
        InvoiceFactory
          .create(invoice)
          .then(function (created) {
            console.log(created);
            return alert('생성하였습니다.');
          })
          .catch(function (err) {
            console.log('ERROR:', err);
          });
      }
    };

    $scope.addEmail = function (email) {
      // check email
      if ( !email ) return;
      if ( !validateEmail(email) ) return;
      if ( isEmailDuplicated($scope.invoice.emails, email) ) return;

      $scope.invoice.emails.push(email);
      $scope.emailAddress = undefined;
    };

    $scope.open = function($event, index) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened[index] = true;
    };

    $scope.selectedProduct = function (item, index) {
      if ( isProductDuplicated($scope.invoice.products, item) ) {
        $scope.addAlert('warning', item.name + '은(는) 이미 등록하셨습니다. 수량을 체크하세요.');
        delete $scope.invoice.products[index].name;
        return;
      }

      $scope.invoice.products[index] = item;
    };

    $scope.getTotal = function () {
      var subTotal = parseInt($scope.getSubTotal()) || 0;
      var discount = parseInt($scope.invoice.discount/100*subTotal) || 0;
      var shipping = parseInt($scope.invoice.shipping) || 0;
      var adjustment = parseInt($scope.invoice.adjustment) || 0;

      return subTotal - discount + shipping + adjustment;
    };

    $scope.getSubTotal = function () {
      var subTotal = 0;

      for ( var i in $scope.invoice.products ) {
        var currentProduct = $scope.invoice.products[i];

        var value = ( currentProduct.tax ) ? currentProduct.price * (1+currentProduct.tax/100) * currentProduct.quantity : currentProduct.price * currentProduct.quantity;

        if ( isNaN(value) ) value = 0;

        subTotal += value;
      }

      return subTotal;
    };

    $scope.selectedCustomer = function (item) {
      $scope.invoice.sentTo = item;
      if ( item.email ) $scope.invoice.emails = [ item.email ];
    };

    $scope.clearItem = function (index) {
      $scope.invoice.products[index] = {};
    };

    $scope.addProduct = function () {
      $scope.invoice.products.push({});
    };

    $scope.deleteProduct = function (index) {
      $scope.invoice.products.splice(index, 1);
    };

    $scope.addAlert = function (type, message) {
      $scope.alerts.push({ type: type, msg: message });
    };

    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };

    // CHECKBOX
    var updateSelected = function(action, id) {
      if (action === 'add' && $scope.selected.indexOf(id) === -1) {
        $scope.selected.push(id);
      }
      if (action === 'remove' && $scope.selected.indexOf(id) !== -1) {
        $scope.selected.splice($scope.selected.indexOf(id), 1);
      }
    };

    $scope.updateSelection = function($event, id) {
      var checkbox = $event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      updateSelected(action, id);
    };

    $scope.selectAll = function($event) {
      var checkbox = $event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      for ( var i = 0; i < $scope.invoices.length; i++) {
        var entity = $scope.invoices[i];
        updateSelected(action, entity.id);
      }
    };

    $scope.getSelectedClass = function(entity) {
      return $scope.isSelected(entity.id) ? 'selected' : '';
    };

    $scope.isSelected = function(id) {
      return $scope.selected.indexOf(id) >= 0;
    };

    $scope.isSelectedAll = function() {
      if ( !$scope.invoices ) return false;
      if ( !$scope.selected ) $scope.selected = [];
      return $scope.selected.length === $scope.invoices.length;
    };
    // CHECKBOX

    function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

    function isEmailDuplicated (emails, email) {
      console.log(emails, email);

      for ( var i in emails ) {
        if ( emails[i] === email ) {
          return true;
        }
      }

      return false;
    }

    function isProductDuplicated (products, selected) {
      for ( var i in products ) {
        if ( products[i].id === selected.id ) {
          return true;
        }
      }

      return false;
    }

    function showInvalidAttributes (err) {
      var alertMessage = '';

      if ( err.data.hasOwnProperty('invalidAttributes') ) {
        for ( var i in err.data.invalidAttributes )
          alertMessage += i + ', ';

        alert(alertMessage);
      } else {
        return alert('견적서 생성 중 오류가 발생하였습니다.');
      }
    };

    function loadingDone () {
      $scope.isLoading = false;

      if ($scope.navless) PageFactory.toggleNavStatus(false);
      else PageFactory.toggleNavStatus(true);

      $scope.htmlReady();
    };
  });
