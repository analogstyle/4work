'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
