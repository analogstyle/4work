'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('LoginCtrl', function ($scope, $rootScope, $routeParams, $location, UserFactory) {
    // console.log('EMAIL:', $routeParams.email, ' TOKEN:', $routeParams.token);

    if ( $routeParams.email && $routeParams.token ) {
      UserFactory
        .loginWithToken($routeParams)
        .then(function (token) {
          console.log('TOKEN:', token);
          return UserFactory.getUser();
        })
        .then(function (user) {
          $rootScope.user = user;

          return $location.path('/home');
        })
        .catch(function (err) {
          console.log('ERROR:', err);
          alert('로그인 중 오류가 발생하였습니다.');
        });
    }

    $scope.tokenRequest = function () {
      UserFactory
        .requestToken($scope.email)
        .then(function () {
          alert('토큰을 요청하였습니다. 이메일을 확인하세요.');
          showTokenLogin();
        });
    };

    $scope.tokenLogin = function () {
      UserFactory
        .loginWithToken($scope.tokenCredentials)
        .then(function (token) {
          console.log('TOKEN:', token);
          return UserFactory.getUser();
        })
        .then(function (user) {
          $rootScope.user = user;

          return $location.path('/home');
        })
        .catch(function (err) {
          console.log('ERROR:', err);
          alert('로그인 중 오류가 발생하였습니다.');
        });
    };

    $scope.showTokenRequest = function () {
      showTokenRequest();
    };

    $scope.showTokenLogin = function () {
      showTokenLogin();
    };

    $scope.login = function () {
      UserFactory
        .login($scope.loginCredentials)
        .then(function (token) {
          return UserFactory.getUser();
        })
        .then(function (user) {
          $rootScope.user = user;

          return $location.path('/home');
        })
        .catch(function (err) {
          console.log('ERROR:', err);
          alert('로그인 중 오류가 발생하였습니다.');
        });
    };

    $scope.signup = function () {
      UserFactory
        .signup($scope.signupCredentials)
        .then(function (user) {
          console.log(user);
          alert('회원가입에 성공하였습니다.');
          $scope.type = 'password';
        })
        .catch(function (err) {
          console.log('ERROR:', err);
          alert('회원가입 중 오류가 발생하였습니다.');
        });
    };

    function errorHandler (err) {
      $scope.errorMsg = err.data;
    };

    function showTokenRequest () {
      angular.element('#token-login').fadeOut(function () {
        angular.element('#token-request').fadeIn();
      });
    };

    function showTokenLogin () {
      angular.element('#token-request').fadeOut(function () {
        angular.element('#token-login').fadeIn();
      });
    };
  });
