'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:ProductCtrl
 * @description
 * # ProductCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('ProductCtrl', function ($scope, $routeParams, $location, ProductFactory) {

    Initiate();

    function Initiate () {
      if ( $routeParams.id ) {
        ProductFactory
          .getOne($routeParams.id).$promise
          .then(function (res) {
            $scope.product = res;
          });
      }

      $scope.products = ProductFactory.getList();
    }

    $scope.goTo = function (id) {
      if ($scope.currentPage)
        $location.search('page', $scope.currentPage);

      $location.path('/product/editor/' + id);
    };

    $scope.goBack = function () {
      $location.path('/product');
    };

    $scope.deleteProduct = function (id) {
      if ( !confirm('정말로 삭제하시겠습니까?') ) return;

      ProductFactory
        .delete({ id: id }).$promise
        .then(function (deleted) {
          for ( var i in $scope.products ) {
            if ( $scope.products[i].id === deleted.id ) {
              $scope.products.splice(i, 1);
              break;
            }
          }
        });
    };

    $scope.saveProduct = function (data) {
      if ( data.id ) {
        return ProductFactory
          .edit(data).$promise
          .then(function (product) {
            $scope.product = product;
            alert('저장되었습니다.');
          })
          .catch(function (err) {
            console.log('ERROR:', err);
          });
      }

      return ProductFactory
        .add(data)
        .then(function (product) {
          $scope.product = product;
          alert('생성되었습니다.');
        })
        .catch(function (err) {
          console.log('ERROR:', err);
        });
    };

    // CHECKBOX
    var updateSelected = function(action, id) {
      if (action === 'add' && $scope.selected.indexOf(id) === -1) {
        $scope.selected.push(id);
      }
      if (action === 'remove' && $scope.selected.indexOf(id) !== -1) {
        $scope.selected.splice($scope.selected.indexOf(id), 1);
      }
    };

    $scope.updateSelection = function($event, id) {
      var checkbox = $event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      updateSelected(action, id);
    };

    $scope.selectAll = function($event) {
      var checkbox = $event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      for ( var i = 0; i < $scope.products.length; i++) {
        var entity = $scope.products[i];
        updateSelected(action, entity.id);
      }
    };

    $scope.getSelectedClass = function(entity) {
      return $scope.isSelected(entity.id) ? 'selected' : '';
    };

    $scope.isSelected = function(id) {
      return $scope.selected.indexOf(id) >= 0;
    };

    $scope.isSelectedAll = function() {
      if ( !$scope.products ) return false;
      if ( !$scope.selected ) $scope.selected = [];
      return $scope.selected.length === $scope.products.length;
    };
    // CHECKBOX

    // $scope.hideEditor = function () {
    //   var $panelBody = angular.element('#editor .panel-body');

    //   if ( $panelBody.is(':visible') )
    //     $panelBody.slideUp();
    //   else
    //     $panelBody.slideDown();
    // }
  });
