'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('ContactCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
