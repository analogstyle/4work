'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:EmailCtrl
 * @description
 * # EmailCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('EmailCtrl', function ($scope, $routeParams, $q, $location, UtilityFactory, UserFactory, EstimateFactory, InvoiceFactory, PaymentFactory, EmailFactory) {

    Initiate();

    $scope.goTo = function (id) {
      if ($scope.currentPage)
        $location.search('page', $scope.currentPage);

      $location.path('/email/view/' + id);
    };

    $scope.goBack = function () {
      $location.path('/email');
    };

    $scope.pageChanged = function () {
      $scope.emails = EmailFactory.getList({ page: $scope.currentPage });
    };

    $scope.send = function () {
      EmailFactory
        .send($scope.email)
        .then(function (email) {
          console.log('SENT:', email);
          alert('이메일을 전송하였습니다.');
          $location.path('/'+$routeParams.type+'/view/' + $routeParams.id);
        })
        .catch(function (err) {
          console.log('ERROR:', err);
          alert('오류가 발생하였습니다.');
        });
    };

    $scope.cancel = function () {
      $location.path('/' + $routeParams.type + '/view/' + $routeParams.id);
    };

    // CHECKBOX
    var updateSelected = function(action, id) {
      if (action === 'add' && $scope.selected.indexOf(id) === -1) {
        $scope.selected.push(id);
      }
      if (action === 'remove' && $scope.selected.indexOf(id) !== -1) {
        $scope.selected.splice($scope.selected.indexOf(id), 1);
      }
    };

    $scope.updateSelection = function($event, id) {
      var checkbox = $event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      updateSelected(action, id);
    };

    $scope.selectAll = function($event) {
      var checkbox = $event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      for ( var i = 0; i < $scope.emails.length; i++) {
        var entity = $scope.emails[i];
        updateSelected(action, entity.id);
      }
    };

    $scope.getSelectedClass = function(entity) {
      return $scope.isSelected(entity.id) ? 'selected' : '';
    };

    $scope.isSelected = function(id) {
      return $scope.selected.indexOf(id) >= 0;
    };

    $scope.isSelectedAll = function() {
      if ( !$scope.emails ) return false;
      if ( !$scope.selected ) $scope.selected = [];
      return $scope.selected.length === $scope.emails.length;
    };
    // CHECKBOX

    function Initiate () {
      var type = $routeParams.type;

      // $scope.currentPage = $routeParams.page;

      if ( type ) {
        var queryArray = [ UserFactory.getUser() ];

        if ( type === 'estimate' )
          queryArray.push(EstimateFactory.getOne($routeParams.id).$promise);
        else if ( type === 'invoice' )
          queryArray.push(InvoiceFactory.getOne($routeParams.id).$promise);
        else if ( type === 'payment' )
          queryArray.push(PaymentFactory.getOne($routeParams.id).$promise);

        $q
          .all(queryArray)
          .then(function (objects) {
            console.log('objects[1]:', objects[1]);
            var user = objects[0];
            var attachment = objects[1];

            var userCheckMessage = checkUser(user);
            if ( userCheckMessage && confirm(userCheckMessage + ' 설정 화면으로 이동할까요?') )
              return $location.path('/settings');

            $scope.email = { from: user.email, attachment: attachment };
            $scope.email.model = $routeParams.type;
            $scope.email.to = ( type === 'payment' ) ? attachment.paidBy.email : attachment.sentTo.email;
            $scope.email.subject = UtilityFactory.generateMailTitle(user, user.company, type);
            $scope.email.content = UtilityFactory.generateMailContent(user, user.company, attachment, type);
          })
          .catch(function (err) {
            console.log('ERROR:', err);
          });
      } else {
        console.log($routeParams.page);

        if ( $routeParams.id )
          $scope.email = EmailFactory.getOne($routeParams.id);

        EmailFactory
          .count().$promise
          .then(function (page) {
            $scope.page = page;
            $scope.currentPage = $routeParams.page;
            $scope.emails = EmailFactory.getList({ page: $scope.currentPage });
          }).catch(function (err) {
            console.log('ERROR:', err);
          });
      }
    }

    function checkUser (user) {
      console.log(user.company);
      if ( user.name <= 0 ) return '유저 이름이 없습니다.';
      if ( !user.company || user.company.name.length <= 0 ) return '회사명을 입력하셔야 합니다.';

      return;
    }
  });
