'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:SettingsCtrl
 * @description
 * # SettingsCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('SettingsCtrl', function ($scope, $rootScope, UtilityFactory, UserFactory) {

    UserFactory
      .getUser()
      .then(function (user) {
        if ( !user.company ) user.company = {};
        if ( !user.company.address ) user.company.address = [];

        $rootScope.user = user;
      })
      .catch(function (err) {
        console.log('ERROR:', err);
      });

    $scope.save = function (data) {
      UserFactory
        .update(data)
        .then(function (user) {
          $rootScope.user = user;
          return alert('설정이 저장되었습니다.');
        })
        .catch(function (err) {
          console.log('ERROR:', err);
        });
    };

    $scope.check = function () {
      UtilityFactory
        .checkPortalDuplicate($scope.user.url)
        .then(function (data) {
          if ( !data.url ) throw new Error('NO_URL_FOUND');

          $scope.user.url = data.url;
          $scope.isValidUrl = true;
        })
        .catch(function (err) {
          console.log('ERROR:', err);
          $scope.isValidUrl = false;
        });
    };
  });
