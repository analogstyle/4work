'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('DashboardCtrl', function ($scope, $filter, UserFactory) {
    UserFactory
      .getStats()
      .then(function (stats) {
        $scope.config = {
          labels: false,
          legend: {
            display: true,
            position: 'right'
          },
          colors: [ '#64BEE5', '#26AC21', '#FD7017', '#FB272A' ],
          lineCurveType: 'cardinal',
        };

        $scope.data = {
          series: [
            $filter('translate')('EMAIL'),
            $filter('translate')('ESTIMATE'),
            $filter('translate')('INVOICE'),
            $filter('translate')('PAYMENTS_RECEIVED')
          ],
          data: []
        };

        for ( var i in stats.emails ) {
          $scope.data.data.push({
            x: i,
            y: [
              stats.emails[i].count,
              stats.estimates[i].count,
              stats.invoices[i].count,
              stats.payments[i].count
            ],
            tooltip: [
              $filter('translate')('EMAIL')+' '+stats.emails[i].count,
              $filter('translate')('ESTIMATE')+' '+stats.estimates[i].count,
              $filter('translate')('INVOICE')+' '+stats.invoices[i].count,
              $filter('translate')('PAYMENTS_RECEIVED')+' '+stats.payments[i].count
            ]
          })
        }

        $scope.stats = {
          customers: stats.customers,
          products: stats.products
        }

      });

    $scope.slides = [
      { title: 'EMAIL_FIRST', text: 'EMAIL_FIRST_CONTENT', icon: 'fa-envelope-o' },
      { title: 'WORK_WITH_CLOUD', text: 'WORK_WITH_CLOUD_CONTENT', icon: 'fa-cloud' },
      { title: 'GET_PAID_FASTER', text: 'GET_PAID_FASTER_CONTENT', icon: 'fa-money' }
    ];

  });
