'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:NavCtrl
 * @description
 * # NavCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('NavCtrl', function ($scope, $rootScope, $location, UserFactory, PageFactory) {

    $scope.PageFactory = PageFactory;

    $scope.toggleSideBar = function () {
      angular.element('.row-offcanvas').toggleClass('active');
      angular.element('html, body').animate({ scrollTop: 0 }, 'slow');
    }

    $scope.toggle = function (id) {
      var $subnav = angular.element('#'+id);
      var $subnavtoggle = angular.element('#'+id+'-toggle');

      if ( $subnav.is(':visible') ) {
        $subnav.slideUp();
        $subnavtoggle.removeClass('fa-caret-down').addClass('fa-caret-right');
      } else {
        $subnav.slideDown();
        $subnavtoggle.removeClass('fa-caret-right').addClass('fa-caret-down');
      }
    }

    $scope.getLocation = getLocation;

    $scope.getUser = function () {
      UserFactory
        .getUser()
        .then(function (user) {
          $rootScope.user = user;
        })
        .catch(function (err) {
          console.log('ERROR:', err);
        });
    }

    $scope.logout = function () {
      UserFactory.logout();
      $rootScope.user = undefined;
      $location.path('/');
    };

    function getLocation () {
      var path = $location.path();
      var splitted = path.split('/');
      splitted.splice(0, 1);

      return splitted;
    };
  });
