'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:PaymentCtrl
 * @description
 * # PaymentCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('PaymentCtrl', function ($scope, $q, $routeParams, $modal, $location, PaymentFactory, PageFactory) {

    Initiate();

    $scope.savePayment = function (data) {
      console.log(data);
    };

    $scope.pageChanged = function () {
      $scope.payments = PaymentFactory.getList({ page: $scope.currentPage });
    };

    $scope.deletePayment = function (id) {
      if ( !confirm('정말로 삭제하시겠습니까?') ) return;

      PaymentFactory
        .delete({ id: id }).$promise
        .then(function (deleted) {
          alert('삭제되었습니다.');
          $location.path('/payment');
        });
    };

    $scope.print = function () {
      console.log('PRINTING...');

      // var printContents = document.getElementById('preview').innerHTML;
      window.print();
      // var popupWin = window.open('', '_blank', 'width=720,height=1024');
      // popupWin.document.open()
      // popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
      // popupWin.document.close();
    };

    $scope.openPaymentForm = function (size) {
      console.log('opening');

      var modalInstance = $modal.open({
        templateUrl: '/views/payment-modal.html',
        controller: 'PaymentModalCtrl',
        size: size,
        resolve: {
          object: function () {
            return $scope.payment;
          }
        }
      });

      modalInstance.result.then(function (payment) {
        $scope.payment = payment;
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    };

    $scope.goTo = function (id) {
      if ($scope.currentPage)
        $location.search('page', $scope.currentPage);

      $location.path('/payment/view/' + id);
    };

    $scope.goBack = function () {
      $location.path('/payment');
    };

    // CALENDAR
    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    // CHECKBOX
    var updateSelected = function(action, id) {
      if (action === 'add' && $scope.selected.indexOf(id) === -1) {
        $scope.selected.push(id);
      }
      if (action === 'remove' && $scope.selected.indexOf(id) !== -1) {
        $scope.selected.splice($scope.selected.indexOf(id), 1);
      }
    };

    $scope.updateSelection = function($event, id) {
      var checkbox = $event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      updateSelected(action, id);
    };

    $scope.selectAll = function($event) {
      var checkbox = $event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      for ( var i = 0; i < $scope.payments.length; i++) {
        var entity = $scope.payments[i];
        updateSelected(action, entity.id);
      }
    };

    $scope.getSelectedClass = function(entity) {
      return $scope.isSelected(entity.id) ? 'selected' : '';
    };

    $scope.isSelected = function(id) {
      return $scope.selected.indexOf(id) >= 0;
    };

    $scope.isSelectedAll = function() {
      if ( !$scope.payments ) return false;
      if ( !$scope.selected ) $scope.selected = [];
      return $scope.selected.length === $scope.payments.length;
    };
    // CHECKBOX

    function Initiate () {
      $scope.isLoading = true;

      $scope.currentPage = $routeParams.page;
      $scope.page = PaymentFactory.count();

      if ( $routeParams.id ) {
        PaymentFactory
          .getOne($routeParams.id).$promise
          .then(function (payment) {
            $scope.payment = payment;

            loadingDone();
          });
      }

      $q
        .all([
          PaymentFactory.getList({ page: $scope.currentPage }),
        ])
        .then(function (objects) {
          $scope.payments = objects[0];

          loadingDone();
        });
    };

    function loadingDone () {
      if ($scope.navless) PageFactory.toggleNavStatus(false);
      else PageFactory.toggleNavStatus(true);

      $scope.isLoading = false;
      $scope.htmlReady();
    };
  });
