'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:CustomerCtrl
 * @description
 * # CustomerCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('CustomerCtrl', function ($scope, $routeParams, $location, CustomerFactory, UtilityFactory) {

    Initiate();

    $scope.pageChanged = function () {
      $scope.customers = CustomerFactory.getList({ page: $scope.currentPage });
    };

    $scope.urlify = function (url) {
      return UtilityFactory.getUrl(url);
    };

    $scope.goTo = function (id) {
      if ($scope.currentPage)
        $location.search('page', $scope.currentPage);

      $location.path('/customer/editor/' + id);
    };

    $scope.goBack = function () {
      $location.path('/customer');
    };

    $scope.toggleAdditionals = function () {
      var $icon = angular.element('#toggle-additionals');
      var $additionals = angular.element('#additional-information');

      if ( $additionals.is(':visible') ) {
        $icon.removeClass('fa-caret-down').addClass('fa-caret-up');
        $additionals.slideUp();
      } else {
        $icon.removeClass('fa-caret-up').addClass('fa-caret-down');
        $additionals.slideDown();
      }
    };

    $scope.deleteCustomer = function (id) {
      if ( !confirm('정말로 삭제하시겠습니까?') ) return;

      CustomerFactory
        .delete({ id: id }).$promise
        .then(function (deleted) {
          for ( var i in $scope.customers ) {
            if ( $scope.customers[i].id === deleted.id ) {
              $scope.customers.splice(i, 1);
              break;
            }
          }
        });
    };

    $scope.saveCustomer = function (data) {
      if ( data.id ) {
        return CustomerFactory
          .edit(data).$promise
          .then(function (customer) {
            $scope.customer = customer;
            alert('저장되었습니다.');
          })
          .catch(function (err) {
            console.log('ERROR:', err);
          });
      }

      return CustomerFactory
        .add(data)
        .then(function (customer) {
          $scope.customer = customer;
          alert('생성되었습니다.');
        })
        .catch(function (err) {
          console.log('ERROR:', err);
        });
    };

    // CHECKBOX
    var updateSelected = function(action, id) {
      if (action === 'add' && $scope.selected.indexOf(id) === -1) {
        $scope.selected.push(id);
      }
      if (action === 'remove' && $scope.selected.indexOf(id) !== -1) {
        $scope.selected.splice($scope.selected.indexOf(id), 1);
      }
    };

    $scope.updateSelection = function($event, id) {
      var checkbox = $event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      updateSelected(action, id);
    };

    $scope.selectAll = function($event) {
      var checkbox = $event.target;
      var action = (checkbox.checked ? 'add' : 'remove');
      for ( var i = 0; i < $scope.customers.length; i++) {
        var entity = $scope.customers[i];
        updateSelected(action, entity.id);
      }
    };

    $scope.getSelectedClass = function(entity) {
      return $scope.isSelected(entity.id) ? 'selected' : '';
    };

    $scope.isSelected = function(id) {
      return $scope.selected.indexOf(id) >= 0;
    };

    $scope.isSelectedAll = function() {
      if ( !$scope.customers ) return false;
      if ( !$scope.selected ) $scope.selected = [];
      return $scope.selected.length === $scope.customers.length;
    };
    // CHECKBOX

    function Initiate () {
      $scope.currentPage = $routeParams.page;
      $scope.page = CustomerFactory.count();

      if ( $routeParams.id ) {
        CustomerFactory
          .getOne($routeParams.id).$promise
          .then(function (customer) {
            if ( !customer.billing ) customer.billing = {};
            if ( !customer.billing.address ) customer.billing.address = [];
            if ( !customer.shipping ) customer.shipping = {};
            if ( !customer.shipping.address ) customer.shipping.address = [];

            $scope.customer = customer;
          });
      }

      $scope.customers = CustomerFactory.getList({ page: $scope.currentPage });
    }
  });
