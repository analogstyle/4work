'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:ReportsCtrl
 * @description
 * # ReportsCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('ReportsCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
