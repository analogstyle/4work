'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:PaymentModalCtrl
 * @description
 * # PaymentModalCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('PaymentModalCtrl', function ($scope, $modalInstance, object, PaymentFactory) {

    if ( !object.hasOwnProperty('no') ) {
      $scope.payment = object;
    } else {
      $scope.invoice = object;
      $scope.payment = {
        amount: object.price,
        paidBy: object.sentTo.id,
        invoice: object.id,
        createdAt: new Date()
      };
    }


    $scope.pay = function () {
      if ( !$scope.paymentForm.$valid )
        return alert('아직 입력할 항목이 더 있습니다.');

      $scope.payment.bankCharges = parseFloat($scope.payment.bankCharges);

      if ( !$scope.payment.id ) {
        PaymentFactory
          .create($scope.payment)
          .then(function (payment) {
            console.log('PAID:', payment);
            $modalInstance.close(payment);
            return alert('결제내용을 기록하였습니다.');
          })
          .catch(function (err) {
            console.log('ERROR:', err);
          });
      } else {
        PaymentFactory
          .update($scope.payment).$promise
          .then(function (payment) {
            console.log('PAID:', payment);
            $modalInstance.close(payment);
            return alert('결제내용을 기록하였습니다.');
          })
          .catch(function (err) {
            console.log('ERROR:', err);
          });
      }
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

  });
