'use strict';

/**
 * @ngdoc function
 * @name 4workApp.controller:EstimateeditorCtrl
 * @description
 * # EstimateeditorCtrl
 * Controller of the 4workApp
 */
angular.module('4workApp')
  .controller('EstimateEditorCtrl', function ($scope, $routeParams, $q, $location, UtilityFactory, EstimateFactory, ProductFactory, CustomerFactory, PageFactory, WageFactory, CalculatorFactory) {

    Initiate();

    $scope.Calculator = CalculatorFactory;

    $scope.selectWage = function (expense, data, item) {
      var dataCounter = 0;

      for ( var i in expense.data ) {
        if ( expense.data[i].rate === item.rate ) dataCounter++;
      }

      if ( dataCounter > 1 ) {
        data.rate = '';
        data.daily = 0;
        data.monthly = 0;

        return alert(item.rate + ' 등급은 이미 등록하셨습니다.');
      }

      data.daily = item.daily;
      data.monthly = item.monthly;
    };

    $scope.addTypeInDirectExpense = function (type) {
      for ( var i in $scope.estimate.expenses.direct ) {
        if ( $scope.estimate.expenses.direct[i].type === type )
          return alert(type + '은 이미 존재하는 항목입니다.');
      }

      $scope.estimate.expenses.direct.push({ type: type, data: [{ number: 0, support: 0, participation: 0 }] });
    };

    $scope.addWageInExpenses = function (wage) {
      wage.number = 1;
      wage.support = 21;
      wage.participation = 100;

      for ( var i in $scope.estimate.expenses.direct ) {
        var isExistingData = false;

        for ( var j in $scope.estimate.expenses.direct[i].data ) {
          if ( $scope.estimate.expenses.direct[i].data[j].rate === wage.rate ) {
            isExistingData = true;
            break;
          }
        }

        if ( !isExistingData ) {
          $scope.estimate.expenses.direct[i].data.push(UtilityFactory.clone(wage));
        }
      }
    };

    $scope.calculateTarget = function () {
      if (! $scope.target ) return;

      return parseInt($scope.target) - $scope.caculateTotal();
    };

    $scope.deleteIndirect = function (index) {
      $scope.estimate.expenses.indirect.splice(index, 1);
    };

    $scope.deleteData = function (object, index) {
      object.data.splice(index, 1);

      if ( object.data.length === 0 ) {
        for ( var i in $scope.estimate.expenses.direct ) {
          if ( $scope.estimate.expenses.direct[i].type === object.type )
            $scope.estimate.expenses.direct.splice(i, 1);
        }
      }
    };

    $scope.addDirectExpense = function (title) {
      for ( var i in $scope.estimate.expenses.direct ) {
        if ( $scope.estimate.expenses.direct[i].type === title ) return alert(title + '은 이미 존재하는 항목입니다.');
      }

      $scope.estimate.expenses.direct.push({ type: title, data: [{ number: 0, support: 0, participation: 0 }] });
    };

    $scope.addIndirectExpense = function (mode) {
      $scope.estimate.expenses.indirect.push({ 'mode': mode });
    };

    $scope.addData = function (index) {
      var directData = $scope.estimate.expenses.direct[index].data;
      directData.push({ number: 1, support: 21, participation: 100 });
    };



    $scope.saveEstimate = function (estimate, send) {
      if ( send ) estimate.isSent = true;
      if ( estimate.type === 'DEVELOPMENT' ) estimate.expenses = $scope.estimate.expenses;

      if ( estimate.id ) {
        return EstimateFactory
          .edit(estimate).$promise
          .then(function (estimate) {
            $scope.estimate = estimate;
            alert('저장되었습니다.');
          })
          .catch(function (err) {
            console.log('ERROR:', err);
          });
      }

      EstimateFactory
        .create(estimate)
        .then(function (created) {
          alert('생성하였습니다.');
          $location.path('/estimate/view/'+created.id);
          return;
        })
        .catch(UtilityFactory.errorHandler);
    };

    $scope.addEmail = function (email) {
      // check email
      if ( !email ) return;
      if ( !UtilityFactory.validateEmail(email) ) return;
      if ( UtilityFactory.isEmailDuplicated($scope.estimate.emails, email) ) return;

      $scope.estimate.emails.push(email);
      $scope.emailAddress = undefined;
    };

    $scope.open = function($event, index) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened[index] = true;
    };

    $scope.selectedProduct = function (item, index) {
      if ( UtilityFactory.isProductDuplicated($scope.estimate.products, item) ) {
        $scope.addAlert('warning', item.name + '은(는) 이미 등록하셨습니다. 수량을 수정하세요.');
        delete $scope.estimate.products[index].name;
        return;
      }

      $scope.estimate.products[index] = item;
    };

    $scope.selectedCustomer = function (item) {
      $scope.estimate.sentTo = item;
      if ( item.email ) $scope.estimate.emails = [ item.email ];
    };

    $scope.clearItem = function (index) {
      $scope.estimate.products[index] = {};
    };

    $scope.addProduct = function () {
      $scope.estimate.products.push({});
    };

    $scope.deleteProduct = function (index) {
      $scope.estimate.products.splice(index, 1);
    };



    $scope.addAlert = function (type, message) {
      $scope.alerts.push({ type: type, msg: message });
    };

    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };

    function Initiate () {
      $scope.isLoading = true;

      $scope.standardWages = WageFactory.getWages();
      $scope.defaultTypes = [ '프로젝트 관리/기획/지원', '분석/설계', '디자인', '코딩' ];
      $scope.estimate = { createdAt: new Date(), products: [{}], emails: [], vatIncluded: true };
      $scope.estimate.expenses = { direct: [], indirect: [] };
      $scope.alerts = [];
      $scope.opened = [];

      var queryArray = [
        EstimateFactory.getEstimateNo(),
        ProductFactory.getList(),
        CustomerFactory.getList()
      ];

      if ( $routeParams.id ) queryArray.push(EstimateFactory.getOne($routeParams.id));

      $q
        .all(queryArray)
        .then(function (objects) {
          $scope.estimate.no = objects[0].no;
          $scope.products = objects[1];
          $scope.customers = objects[2];

          if ( objects[3] ) {
            $scope.estimate = objects[3];

            if ( !$scope.estimate.sentTo )
              $scope.estimate.sentTo = objects[3].sentToName;
          }

          if ( !$scope.estimate.type )
            $scope.estimate.type = 'DEVELOPMENT';

          loadingDone();
        })
        .catch(loadingDone);
    };

    function loadingDone (err) {
      if (err) console.log('ERROR:', err);

      PageFactory.toggleNavStatus(true);

      $scope.isLoading = false;
    };
  });
