'use strict';

/**
 * @ngdoc overview
 * @name 4workApp
 * @description
 * # 4workApp
 *
 * Main module of the application.
 */
angular
  .module('4workApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'pascalprecht.translate',
    'config',
    'seo',
    'ui.bootstrap',
    'textAngular',
    'slick',
    'angularCharts'
  ])

  .config(function ($httpProvider) {
    $httpProvider
      .interceptors.push('AuthInterceptor');
  })

   .config(function ($locationProvider) {
    $locationProvider
      .html5Mode(true)
      .hashPrefix('!');
  })

  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        access: { withoutLogin: true }
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        access: { withoutLogin: true }
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl',
        access: { withoutLogin: true }
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        access: { withoutLogin: true }
      })
      // .when('/login/token', {
      //   templateUrl: 'views/login.html',
      //   controller: 'LoginCtrl',
      //   access: { withoutLogin: true }
      // })
      .when('/agreement', {
        templateUrl: 'views/agreement.html',
        controller: 'LoginCtrl',
        access: { withoutLogin: true }
      })

      // DASHBOARD
      .when('/dashboard', {
        templateUrl: 'views/dashboard.html',
        controller: 'DashboardCtrl'
      })
      .when('/dashboard/start', {
        templateUrl: 'views/start.html',
        controller: 'DashboardCtrl'
      })

      // CUSTOMER
      .when('/customer', {
        templateUrl: 'views/customer.html',
        controller: 'CustomerCtrl'
      })
      .when('/customer/editor', {
        templateUrl: 'views/customer-editor.html',
        controller: 'CustomerCtrl'
      })
      .when('/customer/editor/:id', {
        templateUrl: 'views/customer-editor.html',
        controller: 'CustomerCtrl'
      })

      // PRODUCT
      .when('/product', {
        templateUrl: 'views/product.html',
        controller: 'ProductCtrl'
      })
      .when('/product/editor', {
        templateUrl: 'views/product-editor.html',
        controller: 'ProductCtrl'
      })
      .when('/product/editor/:id', {
        templateUrl: 'views/product-editor.html',
        controller: 'ProductCtrl'
      })

      // ESTIMATE
      .when('/estimate', {
        templateUrl: 'views/estimate.html',
        controller: 'EstimateCtrl'
      })
      .when('/estimate/editor', {
        templateUrl: 'views/estimate-editor.html',
        controller: 'EstimateEditorCtrl'
      })
      .when('/estimate/editor/:id', {
        templateUrl: 'views/estimate-editor.html',
        controller: 'EstimateEditorCtrl'
      })
      .when('/estimate/view/:id', {
        templateUrl: 'views/estimate-viewer.html',
        controller: 'EstimateCtrl'
      })
      .when('/estimate/:id', {
        templateUrl: 'views/estimate-viewer-navless.html',
        controller: 'EstimateCtrl'
      })

      // INVOICE
      .when('/invoice', {
        templateUrl: 'views/invoice.html',
        controller: 'InvoiceCtrl'
      })
      .when('/invoice/editor', {
        templateUrl: 'views/invoice-editor.html',
        controller: 'InvoiceCtrl'
      })
      .when('/invoice/editor/:id', {
        templateUrl: 'views/invoice-editor.html',
        controller: 'InvoiceCtrl'
      })
      .when('/invoice/view/:id', {
        templateUrl: 'views/invoice-viewer.html',
        controller: 'InvoiceCtrl'
      })
      .when('/invoice/:id', {
        templateUrl: 'views/invoice-viewer-navless.html',
        controller: 'InvoiceCtrl'
      })

      // PAYMENT
      .when('/payment', {
        templateUrl: 'views/payment.html',
        controller: 'PaymentCtrl'
      })
      .when('/payment/editor', {
        templateUrl: 'views/payment-editor.html',
        controller: 'PaymentCtrl'
      })
      .when('/payment/editor/:id', {
        templateUrl: 'views/payment-editor.html',
        controller: 'PaymentCtrl'
      })
      .when('/payment/view/:id', {
        templateUrl: 'views/payment-viewer.html',
        controller: 'PaymentCtrl'
      })
      .when('/payment/:id', {
        templateUrl: 'views/payment-viewer-navless.html',
        controller: 'PaymentCtrl'
      })

      // EMAIL
      .when('/email', {
        templateUrl: 'views/email.html',
        controller: 'EmailCtrl'
      })
      .when('/email/view/:id', {
        templateUrl: 'views/email-viewer.html',
        controller: 'EmailCtrl'
      })

      .when('/:type/email/:id', {
        templateUrl: 'views/email-editor.html',
        controller: 'EmailCtrl'
      })

      // SETTINGS
      .when('/settings', {
        templateUrl: 'views/settings.html',
        controller: 'SettingsCtrl'
      })

      .when('/schedule', {
        templateUrl: 'views/schedule.html',
        controller: 'ScheduleCtrl'
      })
      .when('/reports', {
        templateUrl: 'views/reports.html',
        controller: 'ReportsCtrl'
      })
      .when('/inventory', {
        templateUrl: 'views/inventory.html',
        controller: 'InventoryCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })

  .config(function ($translateProvider) {
    $translateProvider.useStaticFilesLoader({
      prefix: 'i18n/',
      suffix: '.json'
    });

    $translateProvider.preferredLanguage('ko');
  })

  .run(function ($rootScope, $location, AuthTokenFactory) {
    $rootScope.$on('$routeChangeStart', function (event, next) {
      if ( !next.access || !next.access.withoutLogin ) {
        if ( !AuthTokenFactory.getToken() )
          $location.path('/');
      }
    });
  });
