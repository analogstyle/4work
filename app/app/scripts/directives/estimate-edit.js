'use strict';

/**
 * @ngdoc directive
 * @name 4workApp.directive:estimateEdit
 * @description
 * # estimateEdit
 */
angular.module('4workApp')
  .directive('estimateEdit', function () {
    return {
      templateUrl: 'views/estimate-edit.html',
      restrict: 'E'
    };
  });
