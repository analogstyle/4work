'use strict';

/**
 * @ngdoc directive
 * @name 4workApp.directive:estimateCompany
 * @description
 * # estimateCompany
 */
angular.module('4workApp')
  .directive('estimateCompany', function () {
    return {
      templateUrl: 'views/estimate-company.html',
      restrict: 'E'
    };
  });
