'use strict';

/**
 * @ngdoc directive
 * @name 4workApp.directive:estimateUser
 * @description
 * # estimateUser
 */
angular.module('4workApp')
  .directive('estimateUser', function () {
    return {
      templateUrl: 'views/estimate-user.html',
      restrict: 'E'
    };
  });
