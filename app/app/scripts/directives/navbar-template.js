'use strict';

/**
 * @ngdoc directive
 * @name 4workApp.directive:navbarTemplate
 * @description
 * # navbarTemplate
 */
angular.module('4workApp')
  .directive('navbarTemplate', function () {
    return {
      templateUrl: 'views/navbar-template.html',
      restrict: 'E',
    };
  });
