'use strict';

/**
 * @ngdoc directive
 * @name 4workApp.directive:estimateView
 * @description
 * # estimateView
 */
angular.module('4workApp')
  .directive('estimateView', function () {
    return {
      templateUrl: 'views/estimate-view.html',
      restrict: 'E'
    };
  });
