'use strict';

/**
 * @ngdoc directive
 * @name 4workApp.directive:sidebarTemplate
 * @description
 * # sidebarTemplate
 */
angular.module('4workApp')
  .directive('sidebarTemplate', function () {
    return {
      templateUrl: 'views/sidebar-template.html',
      restrict: 'E'
    };
  });
