'use strict';

/**
 * @ngdoc directive
 * @name 4workApp.directive:loadingIndicator
 * @description
 * # loadingIndicator
 */
angular.module('4workApp')
  .directive('loadingIndicator', function () {
    return {
      templateUrl: 'views/loading-indicator.html',
      restrict: 'E'
    };
  });
