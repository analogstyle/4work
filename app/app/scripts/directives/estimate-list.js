'use strict';

/**
 * @ngdoc directive
 * @name 4workApp.directive:estimateList
 * @description
 * # estimateList
 */
angular.module('4workApp')
  .directive('estimateList', function () {
    return {
      templateUrl: 'views/estimate-list.html',
      restrict: 'E'
    };
  });
