'use strict';

/**
 * @ngdoc directive
 * @name 4workApp.directive:footerTemplate
 * @description
 * # footerTemplate
 */
angular.module('4workApp')
  .directive('footerTemplate', function () {
    return {
      templateUrl: 'views/footer-template.html',
      restrict: 'E'
    };
  });
