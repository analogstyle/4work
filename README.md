# 4Work

This app is created to have [ZOHO](https://www.zoho.com/invoice/) like web service for Koreans. (Personally to learn angular.js in use)

### Introduction

- Backend: [Sails.js](http://sailsjs.org)
- Frontend [Angular.js](https://angularjs.org)


### Demo

http://4work.flit.kr

### GNU License v3 ###

GNU License