/**
 * PaymentController
 *
 * @description :: Server-side logic for managing Payments
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var _ = require('lodash');

module.exports = _.merge(_.cloneDeep(require('./BaseController')), {

	create: function (req, res)  {
    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        req.body.createdBy = user_id;

        return Payment.create(req.body);
      })
      .then(function (payment) {
        return Invoice.update(req.body.invoice, { 'status': 'PAID' });
      })
      .then(function (invoice) {
        return Payment.findOne(invoice.payment).populateAll();
      })
      .then(function (payment) {
        return res.json(payment);
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  },

  find: function (req, res) {
    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        req.query.createdBy = user_id;

        return Payment.find(req.query).sort({ 'createdAt': -1 }).populateAll();
      })
      .then(function (payments) {
        return res.json(payments);
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  },

  update: function (req, res) {
    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        req.body.createdBy = user_id;

        return Payment.update(req.params.id, req.body);
      })
      .then(function (payments) {
        return Payment.findOne(payments[0].id).populateAll();
      })
      .then(function (payment) {
        return res.json(payment);
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  }
});