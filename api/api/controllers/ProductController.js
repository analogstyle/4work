/**
 * ProductController
 *
 * @description :: Server-side logic for managing Products
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var _ = require('lodash');

var Utility = require('../services/Utility');

module.exports = _.merge(_.cloneDeep(require('./BaseController')), {

	create: function (req, res)  {
    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        req.body.createdBy = user_id;

        return Product.create(req.body);
      })
      .then(function (product) {
        return res.json(product);
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  },

  find: function (req, res) {
    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        req.query.createdBy = user_id;

        return Product.find(req.query).sort({ 'createdAt': -1 });
      })
      .then(function (products) {
        return res.json(products);
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  },

  update: function (req, res) {
    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        req.body.createdBy = user_id;

        return Product.update(req.params.id, req.body);
      })
      .then(function (products) {
        return res.json(products[0]);
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  }
});