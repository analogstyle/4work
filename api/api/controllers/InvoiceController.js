/**
 * InvoiceController
 *
 * @description :: Server-side logic for managing Invoices
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var _ = require('lodash');

module.exports = _.merge(_.cloneDeep(require('./BaseController')), {

  getInvoiceNo: function (req, res) {
    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        return User.findOne(user_id);
      })
      .then(function (user) {
        if ( !user.last.invoice )
          return [Invoice.count({ createdBy: user.id }), user];

        return [user.last.invoice, user];
      })
      .spread(function (no, user) {
        return no;
      })
      .then(function (no) {
        return res.json({ no: Utility.generateUniqueNo(no+1, 'INV') });
      });

    // Utility
    //   .getUserIdFromToken(req.headers.authorization)
    //   .then(function (user_id) {
    //     return Invoice.count({ createdBy: user_id });
    //   })
    //   .then(function (count) {
    //     if ( count <= 0 || isNaN(count) )
    //       return res.json({ no: 'INV-000001' });
    //     else
    //       return res.json({ no: Utility.generateUniqueNo(count+1, 'INV') });
    //   });
  },

	create: function (req, res) {
    console.log(req.body.products);
    
    if ( req.body.products.length === 0 )
      return res.serverError('PRODUCTS_NOT_FOUND');

    if ( req.body.sentTo && req.body.sentTo.id ) {
      req.body.sentTo = req.body.sentTo.id
    } else if ( !req.body.sentToName ) {
      req.body.sentToName = req.body.sentTo;
      delete req.body.sentTo;
    }

    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        req.body.createdBy = user_id;

        return User.findOne(user_id);
      })
      .then(function (user) {
        if ( req.body.no ) return [null, user];

        if ( !user.last.invoice )
          return [Invoice.count({ createdBy: user.id }), user];
        else
          return [user.last.invoice, user];
      })
      .spread(function (no, user) {
        if ( no ) {
          return Invoice.find({ createdBy: user.id, no: req.body.no });
        } else {
          req.body.no = Utility.generateUniqueNo(no+1, 'INV');
          return [];
        }
      })
      .then(function (invoices) {
        if ( invoices.length > 0 ) throw new Error('INVOICE_NUMBER_DUPLICATE');

        // console.log('INV_NO:', req.body);

        return Invoice.create(req.body);
      })
      .then(function (invoice) {
        Utility.updateLastNumber('invoice', invoice.createdBy);

        return res.json(invoice);
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });


    // Utility
    //   .getUserIdFromToken(req.headers.authorization)
    //   .then(function (user_id) {
    //     req.body.createdBy = user_id;

    //     return Invoice.find({ createdBy: user_id, no: req.body.no });
    //   })
    //   .then(function (invoices) {
    //     if ( invoices.length > 0 ) throw new Error('INVOICE_NUMBER_DUPLICATE');

    //     return Invoice.create(req.body);
    //   })
    //   .then(function (invoice) {
    //     return res.json(invoice);
    //   })
    //   .catch(function (err) {
    //     sails.log(err);
    //     return res.serverError(err.message);
    //   });
  },

  find: function (req, res) {
    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        req.query.createdBy = user_id;

        return Invoice.find(req.query).sort({ 'createdAt': -1 }).populateAll();
      })
      .then(function (invoices) {
        return res.json(invoices)
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  },

  update: function (req, res) {
    req.body.createdBy = req.body.createdBy.id
    req.body.sentTo = req.body.sentTo.id

    delete req.body.payment;

    Invoice
      .update(req.params.id, req.body)
      .then(function (invoices) {
        return Invoice.findOne(invoices[0].id).populateAll()
      })
      .then(function (invoice) {
        return res.json(invoice);
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  }
});
