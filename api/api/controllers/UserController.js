/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var Promise = require('q');

var Utility = require('../services/Utility');
var EmailService = require('../services/Email');

module.exports = {
  stats: function (req, res) {
    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (userId) {
        return [
          Utility.getStats(Estimate, userId),
          Utility.getStats(Invoice, userId),
          Utility.getStats(Payment, userId),
          Utility.getStats(Email, userId),
          Customer.count({ createdBy: userId }),
          Product.count({ createdBy: userId })
        ];
      })
      .spread(function (estimates, invoices, payments, emails, customers, products) {
        var result = {
          estimates: estimates,
          invoices: invoices,
          payments: payments,
          emails: emails,
          customers: customers,
          products: products
        };
        return res.json(result);
      })
      .catch(function (err) {
        return res.serverError(err.message);
      });
  },

  login: function (req, res) {
    var result = {};

    User
      .findOne({ 'email': req.body.email })
      .populateAll()
      .then(function (user) {
        if (!user) throw new Error ('USER_NOT_FOUND');

        return [Utility.validatePasswordMatch(req.body.password, user.password), user];
      })
      .spread(function (isSuccess, user) {
        if ( !isSuccess ) throw new Error('PASSWORD_NOT_MATCH');

        return [Token.destroy({ user: user.id }), user];
      })
      .spread(function (destroyed, user) {
        sails.log( ( destroyed.length || 0 )+ ' token(s) has been deleted');

        var token = Utility.generateToken(user.email);
        return Token.create({ id: token, type: 'NORMAL', user: user.id });
      })
      .then(function (token) {
        return res.json(token);
      })
      .catch(function (err) {
        return res.serverError(err.message);
      });
  },

  updateProfile: function (req, res) {
    delete req.body.password;
    delete req.body.createdAt;
    delete req.body.updatedAt;
    delete req.body.id;

    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (userId) {
        if ( !userId ) throw new Error ('TOKEN_NOT_FOUND');

        return User.update(userId, req.body);
      })
      .then(function (users) {
        return res.json(users[0]);
      })
      .catch(function (err) {
        return res.serverError(err.message);
      })
  },

  generateToken: function (req, res) {
    if ( !req.params.hasOwnProperty('email') ) return res.serverError('EMAIL_NOT_FOUND');

    var requestedEmail = req.params.email;

    User
      .findOne({ email: requestedEmail })
      .then(function (user) {
        if ( !user ) return User.create({ email: requestedEmail });

        return user;
      })
      .then(function (user) {
        var token = Utility.generateToken(user.email);

        return Token.create({ id: token, type: 'PASSWORDLESS', user: user.id });
      })
      .then(function(token) {
        if ( !token ) throw new Error('TOKEN_NOT_GENERATED');

        EmailService.sendToken(requestedEmail, token.id);

        return res.json({ 'status': 'TOKEN_SENT' });
      })
      .catch(function (error) {
        console.log('ERROR:', error);

        return res.serverError(error.message);
      });
  },

  loginToken: function (req, res) {
    if ( !req.query.email ) return res.serverError('EMAIL_NOT_FOUND');
    if ( !req.query.token ) return res.serverError('TOKEN_NOT_FOUND');

    User
      .findOne({ email: req.query.email })
      .then(function (user) {
        if ( !user ) throw new Error ('USER_NOT_FOUND');

        return Token.findOne({ id: req.query.token, user: user.id }).populateAll();
      })
      .then(function (token) {
        if ( !token ) throw new Error ('TOKEN_NOT_FOUND');

        if ( token.type === 'NORMAL' ) {
          return Token
            .destroy(token.id)
            .then(function (token) {
              throw new Error ('TOKEN_TYPE_INVALID');
            });
        } else if ( token.type === 'PASSWORDLESS' && token.isUsed ) {
          return Token
            .destroy(token.id)
            .then(function (token) {
              throw new Error ('TOKEN_EXPIRED');
            });
        } else {
          console.log('TOKEN_SUCCESS');

          return Token
            .update({ id: token.id }, { isUsed: true })
            .then(function (token) {
              return res.json(token);
            });
        }
      })
      .catch(function (error) {
        return res.serverError(error.message);
      });
  },

  retrieveToken: function (req, res) {
    Token
      .findOne(req.body.token)
      .populateAll()
      .then(function (token) {
        if ( !token ) throw new Error ('TOKEN_EXPIRED');

        return res.json(token.user);
      })
      .catch(function (err) {
        return res.serverError(err.message);
      });
  },

  checkPortalUrlDuplicate: function (req, res) {
    req.params.url = req.params.url.toLowerCase();

    User
      .findOne({ url: req.params.url })
      .then(function (user) {
        if ( user ) throw new Error('FOUND_DUPLICATE');

        return res.json({ url: req.params.url });
      })
      .catch(function (err) {
        return res.serverError(err.message);
      });
  }
};

