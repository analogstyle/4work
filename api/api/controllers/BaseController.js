/**
 * BaseController.js
 *
 * Base controller for all sails.js controllers. This just contains some common code
 * that every controller uses
 */

'use strict';

var actionUtil = require('sails/lib/hooks/blueprints/actionUtil');

module.exports = {
  /**
   * Generic count action for controller.
   *
   * @param   {Request}   request
   * @param   {Response}  response
   */
  count: function count(req, res) {
    var Model = actionUtil.parseModel(req);
    var criteria = actionUtil.parseCriteria(req);

    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        criteria.createdBy = user_id;

        return Model.count(criteria);
      })
      .then(function (count) {
        return res.ok({ count: count });
      })
      .catch(function (err) {
        return res.negotiate(error);
      });
  }
};