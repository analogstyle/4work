/**
 * EstimateController
 *
 * @description :: Server-side logic for managing Estimates
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var _ = require('lodash');

var Promise = require('q');
var Utility = require('../services/Utility');
var Email = require('../services/Email');

module.exports = _.merge(_.cloneDeep(require('./BaseController')), {

  getEstimateNo: function (req, res) {
    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        return User.findOne(user_id);
      })
      .then(function (user) {
        if ( !user.last.estimate ) return [Estimate.count({ createdBy: user.id }), user];

        return [user.last.estimate, user];
      })
      .spread(function (no, user) {
        return no;
      })
      .then(function (no) {
        return res.json({ no: Utility.generateUniqueNo(no+1, 'EST') });
      });
  },

  create: function (req, res) {
    if ( req.body.sentTo && req.body.sentTo.id ) {
      req.body.sentTo = req.body.sentTo.id
    } else {
      req.body.sentToName = req.body.sentTo;
      delete req.body.sentTo;
    }

    console.log(req.body);

    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        req.body.createdBy = user_id;

        return Estimate.find({ createdBy: user_id, no: req.body.no });
      })
      .then(function (estimates) {
        if ( estimates.length > 0 ) throw new Error('ESTIMATE_NUMBER_DUPLICATE');

        return Estimate.create(req.body);
      })
      .then(function (estimate) {
        Utility.updateLastNumber('estimate', estimate.createdBy);

        return res.json(estimate);
      })
      .catch(function (err) {
        return res.serverError(err.message);
      });
  },

  findOne: function (req, res) {
    Estimate
      .findOne(req.params.id)
      .populateAll()
      .then(function (estimate) {
        // estimate.expenses.direct = Utility.removeZeroCostData(estimate.expenses.direct);

        return res.json(estimate);
      })
      .catch(function (err) {
        return res.serverError(err.message);
      });
  },

  find: function (req, res) {
    var pagination = { page: parseInt(req.query.page) || 1, limit: 10 };
    if ( req.query.hasOwnProperty('page') ) delete req.query.page;

    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        req.query.createdBy = user_id;

        return Estimate.find(req.query).sort({ 'createdAt': -1 }).paginate(pagination).populateAll();
        // return Estimate.find(req.query).sort({ 'createdAt': -1 }).populateAll();
      })
      .then(function (estimates) {
        return res.json(estimates)
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  },

  approve: function (req, res) {
    Estimate
      .update(req.params.id, { status: req.params.status })
      .then(function (estimates) {
        return Estimate.findOne(estimates[0].id).populateAll()
      })
      .then(function (estimate) {
        Email.notify(estimate.createdBy.email, estimate, req.body.status);

        return res.json(estimate);
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  },

  update: function (req, res) {
    if ( req.body.createdBy ) req.body.createdBy = req.body.createdBy.id
    if ( req.body.sentTo ) req.body.sentTo = req.body.sentTo.id

    Estimate
      .update(req.params.id, req.body)
      .then(function (estimates) {
        return Estimate.findOne(estimates[0].id).populateAll()
      })
      .then(function (estimate) {
        return res.json(estimate);
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  },

  pdfify: function (req, res) {
    var phantom = require('phantom');
  },

  createPdf: function (req, res) {
    console.log('wow');

    // var pdf = require('phantomjs-pdf');
    // var options = {
    //   'html' : 'http://4work.dev/estimate/54cc53982f7f1700002798a8',
    //   // 'css' : Path to additional CSS file,
    //   // 'js' : Path to additional JavaScript file,
    //   // 'runnings' : Path to runnings file. Check further below for explanation.,
    //   // 'deleteOnAction' : true/false (Deletes the created temp file once you access it via toBuffer() or toFile())
    // }

    // pdf.convert(options, function(result) {
    //   /* Using a buffer and callback */
    //   result.toBuffer(function(returnedBuffer) {});

    //   /* Using a readable stream */
    //   var stream = result.toStream();

    //   /* Using the temp file path */
    //   var tmpPath = result.getTmpPath();

    //   /* Using the file writer and callback */
    //   result.toFile("../file.pdf", function() {
    //     console.log('done');
    //     return res.json({});
    //   });
    // });
  },
});
