/**
 * EmailController
 *
 * @description :: Server-side logic for managing Emails
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var _ = require('lodash');

var Utility = require('../services/Utility');
var EmailService = require('../services/Email');

module.exports = _.merge(_.cloneDeep(require('./BaseController')), {
	create: function (req, res) {
    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        req.body.createdBy = user_id;
        req.body[req.body.model] = req.body.attachment.id;
        req.body.to = Utility.getArrayFromString(req.body.to);
        req.body.cc = Utility.getArrayFromString(req.body.cc);
        req.body.bcc = Utility.getArrayFromString(req.body.bcc);

        delete req.body.attachment;

        return Email.create(req.body);
      })
      .then(function (email) {
        switch ( req.body.model ) {
          case 'estimate':
            Estimate
              .update(email[req.body.model], { status: 'SENT' })
              .then(function (estimate) { sails.log('ESTIMATE:', estimate); });
            break;
          case 'invoice':
            Invoice
              .update(email[req.body.model], { status: 'SENT' })
              .then(function (invoice) { sails.log('INVOICE:', invoice); });
            break;
        }

        return Email.findOne(email.id).populateAll();
      })
      .then(function (email) {
        EmailService.send(email);

        return res.json(email);
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  },

  find: function (req, res) {
    var pagination = { page: parseInt(req.query.page) || 1, limit: 10 };
    if ( req.query.hasOwnProperty('page') ) delete req.query.page;

    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        req.query.createdBy = user_id;

        // return [Email.count(req.query), Email.find(req.query).sort({ 'createdAt': -1 }).paginate(pagination).populateAll()];
        return Email.find(req.query).sort({ 'createdAt': -1 }).paginate(pagination).populateAll();
      })
      // .spread(function (count, emails) {
      //   return res.json(emails);
      // })
      .then(function (emails) {
        return res.json(emails);
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  },
});