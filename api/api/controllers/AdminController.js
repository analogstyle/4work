/**
 * AdminController
 *
 * @description :: Server-side logic for managing Admins
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	index: function (req, res) {
		return res.view('index.html');
	},

	wage: function (req, res) {
		return res.view('wage.html');
	},
};
