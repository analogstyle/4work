/**
 * CustomerController
 *
 * @description :: Server-side logic for managing Customers
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var _ = require('lodash');

var Promise = require('q');
var Utility = require('../services/Utility');

module.exports = _.merge(_.cloneDeep(require('./BaseController')), {

  create: function (req, res)	{
    if ( !req.body.hasOwnProperty('displayName') || !req.body.displayName )
      req.body.displayName = (req.body.company && req.body.company.name) ? req.body.company.name+'-'+req.body.name : req.body.name
      // req.body.displayName = req.body.company.name + '-' + req.body.name;

    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        req.body.createdBy = user_id;

        return Customer.create(req.body);
      })
      .then(function (customer) {
        return res.json(customer)
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  },

  find: function (req, res) {
    Utility
      .getUserIdFromToken(req.headers.authorization)
      .then(function (user_id) {
        req.query.createdBy = user_id;

        return Customer.find(req.query).sort({ 'createdAt': -1 });
      })
      .then(function (customers) {
        return res.json(customers)
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  },

  update: function (req, res) {
    req.body.createdBy = req.body.createdBy.id

    Customer
      .update(req.params.id, req.body)
      .then(function (customers) {
        return res.json(customers[0]);
      })
      .catch(function (err) {
        sails.log(err);
        return res.serverError(err.message);
      });
  }
});