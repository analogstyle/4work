/**
* Email.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    createdBy: {
      model: 'User',
      required: true
    },

    estimate: {
      model: 'Estimate'
    },
    invoice: {
      model: 'Invoice'
    },
    payment: {
      model: 'Payment'
    },

    model: {
      type: 'STRING'
    },

    from: {
      type: 'Email',
      required: true
    },
    to: {
      type: 'ARRAY',
      required: true
    },
    cc: {
      type: 'ARRAY'
    },
    bcc: {
      type: 'ARRAY'
    },

    subject: {
      type: 'STRING',
      required: true
    },
    content: {
      type: 'STRING',
      required: true
    },
  }
};

