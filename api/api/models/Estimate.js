/**
* Estimate.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var Utility = require('../services/Utility');

module.exports = {

  attributes: {

    createdBy: {
      model: 'User',
      required: true
    },
    sentTo: {
      model: 'Customer',
    },
    sentToName: {
      type: 'STRING'
    },
    type: {
      type: 'STRING',
      enum: [ 'PRODUCT', 'DEVELOPMENT' ]
    },
    invoices: {
      collection: 'Invoice',
      via: 'estimate'
    },
    expiredAt: {
      type: 'DATETIME'
    },

    no: {
      type: 'STRING',
      required: true
    },
    emails: {
      type: 'ARRAY'
    },

    products: {
      type: 'ARRAY',
    },
    expenses: {
      type: 'JSON',
    },

    summary: {
      type: 'JSON',
      required: true
    },
    price: {
      type: 'FLOAT',
    },
    tax: {
      type: 'FLOAT',
    },

    discount: {
      type: 'FLOAT',
    },
    shipping: {
      type: 'FLOAT',
    },
    adjustment: {
      type: 'FLOAT'
    },

    salesPerson: {
      type: 'STRING'
    },

    agreement: {
      type: 'STRING'
    },
    memo: {
      type: 'STRING'
    },

    vatIncluded: {
      type: 'BOOLEAN',
      defaultsTo: true
    },

    status: {
      type: 'STRING',
      enum: [ 'DRAFT', 'SENT', 'APPROVED', 'CANCELLED' ],
      defaultsTo: 'DRAFT'
    }
  },

  beforeValidate: function (values, callback) {
    console.log(values);

    if ( !values.hasOwnProperty('products') && !values.hasOwnProperty('expenses') )
      return callback('NO_DATA_FOUND');

    if ( values.hasOwnProperty('products') )
      values.products = Utility.fillProducts(values.products);

    if ( values.hasOwnProperty('expenses') )
      values.expenses = Utility.fillExpenses(values.expenses);

    values.summary = Utility.getSummary(values);

    return callback();
  }
};
