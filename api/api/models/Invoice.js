/**
* Invoice.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var Utility = require('../services/Utility');

module.exports = {

  attributes: {

    createdBy: {
      model: 'User',
      required: true
    },
    sentTo: {
      model: 'Customer',
    },
    sentToName: {
      type: 'STRING'
    },
    estimate: {
      model: 'Estimate'
    },
    payment: {
      model: 'Payment',
      via: 'invoice'
    },
    // payments: {
    //   collection: 'Payment',
    //   via: 'invoice'
    // },

    no: {
      type: 'STRING',
      required: true
    },

    expiredAt: {
      type: 'DATETIME',
      // required: true
    },

    type: {
      type: 'STRING',
      enum: [ 'NORMAL', 'RECURSIVE' ],
      defaultsTo: 'NORMAL'
    },
    recursive: {
      type: 'JSON'
    },

    emails: {
      type: 'ARRAY'
    },

    products: {
      type: 'ARRAY',
    },
    expenses: {
      type: 'JSON',
    },

    price: {
      type: 'FLOAT',
    },
    discount: {
      type: 'FLOAT',
    },
    shipping: {
      type: 'FLOAT',
    },
    adjustment: {
      type: 'FLOAT'
    },

    salesPerson: {
      type: 'STRING'
    },

    agreement: {
      type: 'STRING'
    },
    memo: {
      type: 'STRING'
    },

    status: {
      type: 'STRING',
      enum: [ 'DRAFT', 'SENT', 'CANCELLED', 'PAID' ],
      defaultsTo: 'DRAFT'
    }
  },

  beforeCreate: function (values, callback) {
    values.price = Utility.getTotalPrice(values);

    return callback();
  }
};
