/**
* Token.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	id: {
      type: 'STRING',
      required: true
    },
    type: {
      type: 'STRING',
      enum: [ 'NORMAL', 'PASSWORDLESS' ],
      required: true
    },
    user: {
      model: 'USER',
      required: true
    },
    isUsed: {
      type: 'BOOLEAN',
      defaultsTo: false
    }
  }
};

