/**
* Product.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    createdBy: {
      model: 'User',
      required: true
    },

    name: {
      type: 'STRING',
      required: true
    },
    price: {
      type: 'FLOAT',
      required: true
    },
    tax: {
      type: 'FLOAT',
    },
    unit: {
      type: 'STRING',
    },
    description: {
      type: 'STRING',
    }
  }
};

