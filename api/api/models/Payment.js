/**
* Payment.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    createdBy: {
      model: 'User',
      required: true
    },
    paidBy: {
      model: 'Customer',
      required: true
    },
    invoice: {
      model: 'Invoice',
      via: 'payment',
      required: true
    },

    type: {
      type: 'STRING',
      enum: [ 'CASH', 'CREDIT', 'BANK' ]
    },
    amount: {
      type: 'FLOAT',
      required: true
    },
    charge: {
      type: 'FLOAT'
    },
    description: {
      type: 'STRING'
    },
    sendThank: {
      type: 'BOOLEAN'
    },
    isTaxDeducted: {
      type: 'BOOLEAN'
    },
    tax: {
      type: 'FLOAT',
    },
  }
};
