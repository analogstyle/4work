/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var bcrypt = require('bcrypt');
var Promise = require('q');

module.exports = {

  attributes: {

    customers: {
      collection: 'Customer',
      via: 'createdBy'
    },
    estimates: {
      collection: 'Estimate',
      via: 'createdBy'
    },
    partners: {           // 친구 등록하도록 하자
      collection: 'User',
      via: 'id'
    },

    // DATA
    name: {
      type: 'STRING'
    },
    email: {
      type: 'EMAIL',
      required: true,
      unique: true
    },
    url: {
      type: 'STRING',
      unique: true
    },
    password: {
      type: 'STRING',
      required: true
    },
    phone: {
      type: 'STRING'
    },

    company: {
      type: 'JSON'
    },

    last: {
      type: 'JSON',
      defaultsTo: { estimate: 0, invoice: 0 }
    },
  },

  beforeCreate: function (values, callback) {
    Utility.getPasswordHash(values.password)
      .then(function (hash) {
        values.password = hash;

        return callback();
      });
  },
};
