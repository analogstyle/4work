/**
* Wage.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    type: {
      type: 'STRING'
    },
    year: {
      type: 'STRING'
    },
    data: {
      type: 'ARRAY'
    }
  }
};


//
// {
//   type: '기술사',
//   year: '2015',
//   wage: 408995
// }
