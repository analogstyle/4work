/**
* Customer.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    // APPLICATION
    createdBy: {
      model: 'User',
      required: true
    },

    // DATA
    name: {
      type: 'STRING',
      required: true
    },
    displayName: {
      type: 'STRING',
      required: true
    },
    email: {
      type: 'EMAIL'
    },
    phone: {
      type: 'STRING'
    },
    website: {
      type: 'STRING'
    },
    memo: {
      type: 'STRING'
    },

    payment: {
      type: 'JSON'
    },
    company: {
      type: 'JSON'
    },
    billing: {
      type: 'JSON'
    },
    shipping: {
      type: 'JSON'
    }
  }

};

