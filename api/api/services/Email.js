/**
 * EmailService
 *
 * @description :: Email service for global use
 * @help        :: See http://links.sailsjs.org/docs/services
 */

var path = require('path');
var nodemailer = require('nodemailer');
var templatesDir = path.resolve(__dirname, '../..', 'templates');
var emailTemplates = require('email-templates');

var config = {
  auth: sails.config.project.mailer.auth,
  sender: sails.config.project.mailer.sender
};

// create reusable transporter object using SMTP transport
var transporter = nodemailer.createTransport({
    service: sails.config.project.mailer.service,
    auth: config.auth
});

var template;
emailTemplates(templatesDir, function(err, tmplt) {
  if (err) return console.log(err);
  template = tmplt;
});

module.exports = {
  send: function (maildata) {
    var data = { content: maildata.content };

    template('to-customer', data, function (err, html, text) {
      if (err) return console.log(err);

      var mailOptions = {
        from: maildata.createdBy.name + ' <'+ maildata.from + '>',
        to: maildata.to,
        cc: maildata.cc,
        bcc: maildata.bcc,
        subject: maildata.subject,
        html: html
      };

      transporter.sendMail(mailOptions, function (err, info) {
        if (err) return sails.log(err);
        else return sails.log('Message sent: ' + info.response);
      });
    });

    // var mailOptions = {
    //   from: maildata.createdBy.name + ' <'+ maildata.from + '>',
    //   to: maildata.to,
    //   cc: maildata.cc,
    //   bcc: maildata.bcc,
    //   subject: maildata.subject,
    //   html: maildata.content
    // };

    // console.log(mailOptions);

    // transporter.sendMail(mailOptions, function (err, info) {
    //   if (err) return sails.log(err);
    //   else return sails.log('Message sent: ' + info.response);
    // });
  },

  sendToken: function (email, token) {
    var data = { email: email, token: token, today: new Date() };

    template('token', data, function (err, html, text) {
      if (err) return console.log(err);

      var mailOptions = {
        from: config.sender,
        to: email,
        subject: '4WORK에서 로그인 토큰을 보냈습니다.',
        html: html
      };

      transporter.sendMail(mailOptions, function (err, info) {
        if (err) return sails.log(err);
        else return sails.log('Message sent: ' + info.response);
      });
    });


    // var link = '<a href="http://www.4work.kr/token/login?email=' +email+ '&token=' +token+ '">링크</a>';
    // var content = '';
    //     content += '<h1>로그인 토큰을 받았습니다.</h1>';
    //     content += '<p>로그인 토큰을 요청한 기억이 없으시면, 이 메일을 답장으로 보내주세요.</p>';
    //     content += '<h3>' + token + '</h3>';
    //     // content += '<p>MINIMO 비밀번호 없이 로그인 화면에서 이메일과 토큰을 입력하시거나,</p>';
    //     // content += '<p>다음 ' +link+ '를 클릭하세요.</p>';

    // var mailOptions = {
    //   from: config.sender,
    //   to: email,
    //   subject: subject,
    //   html: content
    // };

    // transporter.sendMail(mailOptions, function (err, info) {
    //   if (err) return sails.log(err);
    //   else return sails.log('Message sent: ' + info.response);
    // });
  },

  notify: function (email, estimate, status) {
    var data = { estimate: estimate, status: status };
    var type = '';
    if ( status === 'APPROVED') type = '승인';
    else if ( status === 'CANCELLED') type = '반려';

    template('estimate-status', data, function (err, html, text) {
      if (err) return console.log(err);

      var mailOptions = {
        from: config.sender,
        to: email,
        subject: ( estimate.sentTo.name || estimate.sentToName )+'고객님께서 견적서를 '+type +'하셨습니다.',
        html: html
      };

      transporter.sendMail(mailOptions, function (err, info) {
        if (err) return sails.log(err);
        else return sails.log('Message sent: ' + info.response);
      });
    });

    // template('estimate-status', data)
    // .then(function (html, text) {
    //   console.log(html);
    //   console.log(text);
    // });

    // var customer = estimate.sentTo;

    // var type = '';
    // if ( status === 'APPROVED') type = '승인';
    // if ( status === 'CANCELLED') type = '반려';

    // var link = '<a href="http://www.4work.kr">링크</a>';
    // var subject = customer.name+'고객님께서 견적서를 '+type +'하셨습니다.';
    // var content = '';
    //     content += '<h1>'+customer.name+'고객님께서 견적서를 '+type +'하셨습니다.</h1>';
    //     content += '<p>발송이 잘못되었거나 고객측 미스 클릭이 있을 수 있으니, ';
    //     content += '필요한 경우 유선으로 확인하시고 진행하시기 바랍니다.</p>';
    //     content += '<h3>고객정보: ';
    //     content += customer.name + ' ' + customer.email;
    //     content += ( customer.phone ) ? ' ' + customer.phone + '<br/>' : '<br/>';
    //     content += '견적서: ' + estimate.no + '</h3>';
    //     content += '<p>확인하시려면 4WORK 견적서 항목에서 확인하시거나,</p>';
    //     content += '<p>다음 ' +link+ '를 클릭하세요.</p>';

    // var mailOptions = {
    //   from: config.sender,
    //   to: email,
    //   subject: subject,
    //   html: content
    // };

    // transporter.sendMail(mailOptions, function (err, info) {
    //   if (err) return sails.log(err);
    //   else return sails.log('Message sent: ' + info.response);
    // });
  },

  // sendPassword: function (email, password) {
  //   var link = '<a href="http://www.minimo.kr/">링크</a>';
  //   var mailContentHtml = '';
  //       mailContentHtml += '<h1>사용자의 요청으로 MINIMO 계정 비밀번호를 재설정하였습니다.</h1>';
  //       mailContentHtml += '<p>재설정 요청한 기억이 없으시면, 이 메일을 답장으로 보내주세요.</p>';
  //       mailContentHtml += '<h3>' + password + '</h3>';
  //       mailContentHtml += '<p>아래 링크를 통해 MINIMO 로 이동하여 비밀번호를 입력하여 사용하시기 바랍니다.</p>';
  //       mailContentHtml += '<p>다음 ' +link+ '를 클릭하세요.</p>';

  //   var mailOptions = {
  //     from: config.sender, // sender address
  //     to: email, // send to self
  //     subject: 'MINIMO 비밀번호가 재설정되었습니다.', // Subject line
  //     html: mailContentHtml // html body
  //   };

  //   transporter.sendMail(mailOptions, function (err, info) {
  //     if (err) return sails.log(err);
  //     else return sails.log('Message sent: ' + info.response);
  //   });
  // }
};
