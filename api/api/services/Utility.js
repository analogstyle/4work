/**
 * UtilityService
 *
 * @description :: Utility service for global use
 * @help        :: See http://links.sailsjs.org/docs/services
 */

var Promise = require('q');
var bcrypt = require('bcrypt');
var Hashids = require('hashids');
var ObjectId = require('sails-mongo/node_modules/mongodb').ObjectID;

module.exports = {

  removeZeroCostData: function (direct) {
    for ( var i in direct ) {
      var data = [];
      for ( var j in direct[i].data ) {
        if ( direct[i].data[j].cost !== 0 ) {
          data.push(direct[i].data[j]);
        }
      }
      direct[i].data = data;
    }

    return direct;
  },

  updateLastNumber: function (type, id) {
    User
      .findOne(id)
      .then(function (user) {
        if ( user.last[type]) {
          return [ user.last[type]+1, user ];
        } else {
          switch (type) {
            case 'estimate':
              return [ Estimate.count({ createdBy: user.id }), user ];
              break;
            case 'invoice':
              return [ Invoice.count({ createdBy: user.id }), user ];
              break;
          }
        }
      })
      .spread(function (no, user) {
        user.last[type]++;
        return user.save();
      })
      .then(function (user) {
        console.log('LAST:', user.last);
      });
  },

  getSummary: function (estimate) {
    var summary = {};

    var subTotal = 0;
    var total = 0;

    if ( estimate.type === 'PRODUCT' ) {
      if ( estimate.products.length === 0 ) return estimate;

      for ( var i in estimate.products ) {
        subTotal += estimate.products[i].cost;
      }

      var discount = parseInt(estimate.discount/100*subTotal) || 0;
      var shipping = parseInt(estimate.shipping) || 0;
      var adjustment = parseInt(estimate.adjustment) || 0;
      total = subTotal + shipping - discount - adjustment;

      summary = {
        subTotal: subTotal,
        total: total
      };
    } else if ( estimate.type === 'DEVELOPMENT' ) {
      if ( estimate.expenses.length === 0 ) return estimate;

      // DIRECT
      var directExpense = 0;
      var indirectExpense = 0;
      var support = 0;
      var number = 0;

      for ( var i in estimate.expenses.direct ) {
        var currentDirect = estimate.expenses.direct[i];

        for ( var j in currentDirect.data ) {
          var currentData = currentDirect.data[j];

          directExpense += currentData.cost;
          support += currentData.support;
          number += currentData.number;
        }
      }

      // INDIRECT
      for ( var i in estimate.expenses.indirect ) {
        var currentIndirect = estimate.expenses.indirect[i];

        indirectExpense += currentIndirect.cost;
      }

      subTotal = directExpense + indirectExpense;

      var discount = parseInt(estimate.discount/100*subTotal) || 0;
      var shipping = parseInt(estimate.shipping) || 0;
      var adjustment = parseInt(estimate.adjustment) || 0;
      var rawTotal = subTotal + shipping - discount - adjustment;

      if ( estimate.vatIncluded ) {
        total = parseInt( rawTotal / 1.1 );
        vat = parseInt( rawTotal - total );
      } else {
        total = parseInt( rawTotal );
        vat = parseInt( rawTotal * 0.1 );
      }

      summary = {
        direct: directExpense,
        indirect: indirectExpense,
        subTotal: subTotal,
        price: total+vat,
        total: total,
        vat: vat,
        support: support,
        number: number
      }
    }

    return summary;
  },

  fillProducts: function (products) {
    if ( products.length === 0 ) return products;

    for ( var i in products ) {
      var currentProduct = products[i];

      currentProduct.cost = currentProduct.price * currentProduct.quantity * ( 1 + currentProduct.tax/100 );
    }

    return products;
  },

  fillExpenses: function (expenses) {
    if ( expenses.direct.length === 0 && expenses.indirect.length === 0 ) return expenses;

    var directExpense = 0;

    for ( var i in expenses.direct ) {
      var currentDirect = expenses.direct[i];

      for ( var j in currentDirect.data ) {
        var currentData = currentDirect.data[j];
        currentData.cost = currentData.daily * currentData.number * currentData.support * currentData.participation/100;
        directExpense += currentData.cost;
      }
    }

    for ( var i in expenses.indirect ) {
      var currentIndirect = expenses.indirect[i];

      if ( currentIndirect.mode === 'RATIO' )
        expenses.indirect[i].cost = directExpense * currentIndirect.ratio/100;
      else
        expenses.indirect[i].cost = parseInt(currentIndirect.amount);
    }

    return expenses;
  },

  getLastWeek: function () {
    var today = new Date();
    var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 6);

    return lastWeek;
  },

  getWeekArray: function () {
    var today = new Date();
    var weekArray = [
      new Date(today.getFullYear(), today.getMonth(), today.getDate() - 6),
      new Date(today.getFullYear(), today.getMonth(), today.getDate() - 5),
      new Date(today.getFullYear(), today.getMonth(), today.getDate() - 4),
      new Date(today.getFullYear(), today.getMonth(), today.getDate() - 3),
      new Date(today.getFullYear(), today.getMonth(), today.getDate() - 2),
      new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1),
      new Date(today.getFullYear(), today.getMonth(), today.getDate()),
    ];

    return weekArray ;
  },

  getArrayFromString: function (string) {
    if ( !string ) return [];

    var array = string.split(',');

    for ( var i in array )
      array[i] = array[i].trim();

    return array;
  },

  generateUniqueNo: function (index, prefix) {
    var generated = prefix + '-';
    var indexLength = index.toString().length;

    for ( var i = 6-indexLength; i > 0; i-- ) {
      generated += '0';
    }

    return generated + index;
  },

  getSubTotal: function (products) {
    var subTotal = 0;

    for ( var i in products ) {
      var currentProduct = products[i];

      var value = ( currentProduct.tax ) ? currentProduct.price * (1+currentProduct.tax/100) * currentProduct.quantity : currentProduct.price * currentProduct.quantity;

      if ( isNaN(value) ) value = 0;

      subTotal += value;
    }

    return subTotal;
  },

  getTotalPrice: function (estimate) {
    var subTotal = parseInt(this.getSubTotal(estimate.products)) || 0;
    var discount = parseInt(estimate.discount/100*subTotal) || 0;
    var shipping = parseInt(estimate.shipping) || 0;
    var adjustment = parseInt(estimate.adjustment) || 0;

    return subTotal - discount + shipping + adjustment;
  },

  getTotalExpense: function (estimate) {
    var directExpense = 0;

    for ( var i in estimate.expenses.direct ) {
      var currentDirect = estimate.expenses.direct[i];
      for ( var j in currentDirect.data ) {
        var currentData = currentDirect.data[j];
        directExpense += currentData.daily * currentData.number * currentData.support * currentData.participation/100;
      }
    }

    var indirectExpense = 0;

    for ( var i in estimate.expenses.indirect ) {
      var currentIndirect = estimate.expenses.indirect[i];
      if ( currentIndirect.mode === 'RATIO' )
        indirectExpense += directExpense * currentIndirect.ratio/100;
      else
        indirectExpense += parseInt(currentIndirect.amount);
    }

    if ( estimate.vatIncluded )
      return ( directExpense + indirectExpense );
    else
      return ( directExpense + indirectExpense ) * 0.9;
  },

  getTax: function (estimate) {
    return ( estimate.price ) * 0.1;
  },

  getUserIdFromToken: function (authorization) {
    return Token
      .findOne(authorization.split(' ')[1])
      .then(function (token) {
        return token.user;
      })
      .catch(function (err) {
        throw new Error(err.message);
      });
  },

  getPasswordHash: function (password) {
    var deferred = Promise.defer();

    bcrypt.genSalt(10, function (err, salt) {
      if (err) deferred.reject(new Error(err));

      bcrypt.hash(password, salt, function (err, hash) {
        if (err) deferred.reject(new Error(err));
        if (!hash) deferred.reject(new Error('HASH_NOT_GENERATED'));

        deferred.resolve(hash);
      });
    });

    return deferred.promise;
  },

  validatePasswordMatch: function (currentPassword, dbPassword) {
    var deferred = Promise.defer();

    if ( !currentPassword ) deferred.reject(new Error('PASSWORD_NOT_ENTERED'));

    bcrypt.compare(currentPassword, dbPassword, function(err, isSuccess) {
      if (err) return deferred.reject(new Error(err));
      if (!isSuccess) deferred.reject(new Error('PASSWORD_NOT_MATCH'));

      deferred.resolve(true);
    });

    return deferred.promise;
  },

  getRandomString: function () {
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';

    for (var i=0; i<string_length; i++) {
      var rnum = Math.floor(Math.random() * chars.length);
      randomstring += chars.substring(rnum,rnum+1);
    }

    return randomstring;
  },

  getPagesFromCount: function (count) {
    var count = ( count/10 <= 1 ) ? 1 : Math.ceil(count/10);

    return count;
  },

  generateToken: function (email) {
    var hashid = new Hashids(email);
    var token = hashid.encode(new Date().getTime());

    return token;
  },

  getStats: function (model, id) {
    var deferred = Promise.defer();
    var lastWeekDay = this.getLastWeek();
    var lastWeekArray = this.getWeekArray();

    if ( !model ) deferred.reject(new Error('COLLECTION_NOT_FOUND'));

    // AGGREGATE OPTIONS
    var match = {
      '$match': {
        'createdAt': { '$gt': lastWeekDay },
        'createdBy': new ObjectId(id)
      }
    };

    var proj1 = {
      '$project': {
        '_id': 0,
        'createdAt': {
          '$add' : [
            '$createdAt', 9 * 60 * 60 * 1000
          ]
        },
        // 'createdBy': 1,

        'h': { '$hour': '$createdAt' },
        'm': { '$minute': '$createdAt' },
        's': { '$second': '$createdAt' },
        'ml': { '$millisecond': '$createdAt' }
      }
    };

    var proj2 = {
      '$project': {
        '_id': 0,
        // 'createdBy': 1,
        'createdAt': {
          '$subtract':[
            '$createdAt',
            {
              '$add': [
                '$ml',
                { '$multiply': [ '$s', 1000 ] },
                { '$multiply': [ '$m', 60, 1000 ] },
                { '$multiply': [ '$h', 114, 60, 1000 ] }
              ]
            }
          ],
        }
      }
    };

    var group = {
      '$group': {
        '_id': { 'createdAt': '$createdAt' },
        'count': { '$sum' : 1 }
      }
    };
    // AGGREGATE OPTIONS

    model.native(function (err, collection) {
      if (err) return deferred.reject(new Error(err));

      collection.aggregate(match, proj1, proj2, group, function (err, stats) {
        if (err) return deferred.reject(new Error(err));

        var statObject = {};

        for ( var i in lastWeekArray ) {
          var theDate = getFormattedDate(lastWeekArray[i]);
          statObject[theDate] = { count: 0 };
        }

        for ( var i in stats ) {
          statObject[getFormattedDate(stats[i]._id.createdAt)] = stats[i];
        }

        console.log(statObject);

        deferred.resolve(statObject);
      });
    })

    return deferred.promise;
  },
};

function getFormattedDate (date) {
  var year = date.getFullYear();
  var month = (1 + date.getMonth()).toString();
  var day = date.getDate().toString();

  month = month.length > 1 ? month : '0' + month;
  day = day.length > 1 ? day : '0' + day;

  return year + '-' + month + '-' + day;
}
